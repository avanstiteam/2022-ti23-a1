#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs_flash.h"
#include "esp_log.h"

#include "sdkconfig.h"

#include "menu.h"
#include "lcd.h"
#include "i2c_conf.h"
#include "radio.h"
#include "wifi.h"
#include "ntp.h"
#include "clock.h"
#include "extgpio.h"
#include "sd_sound_player.h"
#include "audio_init.h"
#include "audio_recognition.h"
#include "audio_handeler.h"
#include "common.h"
#include "TouchButtons.h"
#include "audio_controls.h"
#include "radio.h"
#include "translation_system.h"
#include "amplitude-processing.h"
#include "alarm_clock.h"
#include "timer_clock.h"
#include "morsemenu.h"

#ifdef CONFIG_MENU_USE_BUTTONS

    #include "button_menu_controller.h"

#else

    #include "rotary_encoder.h"

#endif

void move(int16_t val);
void press();
void playWelcomeMessage();
void wakeUp();
void updateVolume(int vol);
void skipAudio();
void pauseAudio();

void got_ip_callback(esp_ip4_addr_t ip) {
    printf("Ip, lol => " IPSTR "\n", IP2STR(&ip));

    ntp_force_sync();
}

void app_main(void)
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }

    // Get selected language.
    int selected_language = get_selected_language();
    printf("Language selected: %d \n", selected_language);

    wifi_init();
    wifi_register_got_ip_handler(got_ip_callback);
    // Call is blocking...
    wifi_connect();
    
    audio_init_initialize();
    audio_handler_pipeline_init();
    menu_init(selected_language);
    audio_recognition_setOnClap(radio_onClap);
    clock_init(selected_language);
    

    extgpio_init();

    

#ifdef CONFIG_MENU_USE_BUTTONS 

    button_menu_controller_set_on_move(move);
    button_menu_controller_set_on_click(press);
    button_menu_controller_init();

#else

    rotary_encoder_init();
    rotary_encoder_setOnMove(move);
    rotary_encoder_setOnClick(press);
    rotary_encoder_startTask();

#endif

    button_audio_controller_init();
    audio_menu_set_volume(updateVolume);
    audio_menu_skip(skipAudio);
    audio_menu_pause(pauseAudio);

    menu_draw((void *)0);
}

void move(int16_t val) {
    if (get_settint_time())
    {
        update_alarm_time(val);
    } else if (get_setting_timer())
    {
        update_timer_time(val);
    } 
    else if(morse_menu_hook_input()) {
        morse_menu_move(val);
    }
    else {
        menu_changePos(val);
    }
}

void press() {
    if (get_settint_time())
    {
        change_hour_minute_state();
    } else if (get_setting_timer())
    {
        change_minute_second_state();
    } 
    else if(morse_menu_hook_input()) {
        morse_menu_press();
    }
    else {
        menu_checkMethod();
    }
}

void updateVolume(int vol) {
    printf("Change vol: %d \n", vol);
    radio_changeVolume(vol);
}

void skipAudio() {
    printf("Skip \n");
    radio_skipStation(1);
    audio_handler_playAudio(AUDIO_PIPE_USERADIO, AAC_STREAM_URI);
}

void pauseAudio() {
    if (return_alarm_went_off() || return_timer_went_off()) {
        printf("Radio stop\n");
        audio_handler_stopAudio();
        audio_handler_playAudio(AUDIO_PIPE_USEVOICE, "");
    } else {
        printf("Pause \n");
        audio_handler_muteRadio();
    }
}

