
#ifndef COMMON_H
#define COMMON_H

#include "freertos/freeRTOS.h"

#define LOCK(semaphore, code) xSemaphoreTake(semaphore, portMAX_DELAY); \
    code; \
    xSemaphoreGive(semaphore);

typedef void (*EventHandler_t)(void);

void task_run_after(TaskFunction_t task, TickType_t time, void* context, uint32_t stackSize);

int bound_loop(int value, int min, int max);

#endif
