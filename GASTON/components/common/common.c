
#include <stdio.h>

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "common.h"

typedef struct {
    TaskFunction_t taskToRun;
    TickType_t delayTime;
    void* context;
} TaskRunAfterParams_t;

static void run_after_task(void* vParams);

/**
 * @brief Runs the given task after the specified amount of time.
 * 
 * @param task The task to run
 * @param time The delay time to wait before running
 * @param context Extra context for the task, will be passed as argument
 */
void task_run_after(TaskFunction_t task, TickType_t time, void* context, uint32_t stackSize) {
    TaskRunAfterParams_t* params = (TaskRunAfterParams_t*) malloc(sizeof(TaskRunAfterParams_t));
    if(params == NULL) {
        ESP_LOGE("task_run_after", "Could not run run_after_task -> malloc params failed");
    }

    params->taskToRun = task;
    params->delayTime = time;
    params->context = context;

    xTaskCreate(run_after_task, "Delayed Task", stackSize, params, 1, NULL);;
}

/**
 * Makes sure that value is between min and max (inclusive).
 * Will loop around if value > max or value < min
 */
int bound_loop(int value, int min, int max) {
    int result = value;

    while (result > max) result = result - max;
    while (result < min) result = result + max;

    return result;
}

static void run_after_task(void* vParams) {
    TaskRunAfterParams_t* params = (TaskRunAfterParams_t*)vParams;

    vTaskDelay(params->delayTime);
    (*params->taskToRun)(params->context);

    vTaskDelete(NULL);
}