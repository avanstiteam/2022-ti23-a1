#include <stdio.h>
#include "rotary_encoder.h"
#include "qwiic_twist.h"

#ifndef LYRAT_SCK
#define LYRAT_SCK 23
#endif
#ifndef LYRAT_SDA
#define LYRAT_SDA 18
#endif

#ifndef I2C_MASTER_NUM
#define I2C_MASTER_NUM           I2C_NUM_0
#endif
#ifndef I2C_MASTER_TX_BUF_LEN
#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#endif
#ifndef I2C_MASTER_RX_BUF_LEN
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled
#endif
#ifndef I2C_MASTER_FREQ_HZ
#define I2C_MASTER_FREQ_HZ       100000
#endif
#ifndef I2C_MASTER_SDA_IO
#define I2C_MASTER_SDA_IO        LYRAT_SDA
#endif
#ifndef I2C_MASTER_SCL_IO
#define I2C_MASTER_SCL_IO        LYRAT_SCK
#endif
#ifndef LYRAT_SCK_SPEED
#define LYRAT_SCK_SPEED 10000
#endif

#define I2C_TWIST 0x3F


qwiic_twist_t twist_info;

// void move(int16_t val);

void rotary_encoder_init()
{

    twist_info.i2c_addr = I2C_TWIST;
    twist_info.port = I2C_MASTER_NUM;
    ESP_ERROR_CHECK(qwiic_twist_init(&twist_info));
    //qwiic_twist_init(&twist_info);

}

void rotary_encoder_setOnMove(void (*onMove)(int16_t)) {
    twist_info.onMoved = onMove;
}
void rotary_encoder_setOnClick(void(*func)(void)) {
    twist_info.onButtonClicked = func;
}
void rotary_encoder_setOnPressed(void(*func)(void)) {
    twist_info.onButtonPressed = func;
}

void rotary_encoder_startTask() {
    qwiic_twist_start_task(&twist_info);
}

void rotary_encoder_stopTask() {
    qwiic_twist_stop_task(&twist_info);
}

