
#ifndef MORSETREE_H
#define MORSETREE_H

#include <stdlib.h>

typedef struct morse_node morse_node_t;
typedef morse_node_t* morse_tree_handle_t;

morse_tree_handle_t morse_tree_build();
char morse_tree_get_char(morse_tree_handle_t tree, char morseBuffer[], int bufferLength);

#endif
