
#ifndef MORSEMENU_H
#define MORSEMENU_H

#include <stdint.h>

void morse_menu_show();
bool morse_menu_hook_input();
void morse_menu_exit();

void morse_menu_move(int16_t val);
void morse_menu_press();

void morse_menu_start_detect();
void morse_menu_stop_detect();

#endif
