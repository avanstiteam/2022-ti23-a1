
#ifndef MORSECODE_H
#define MORSECODE_H

#include "audio_pipeline.h"

typedef struct {

    /**
     * Morsecode speed in Words Per Minute
     * Min=10, Max=30
     */
    char speed;

    /**
     * Frequency in Hertz
     */
    short frequency;

} morse_config_t;

void morse_init(audio_pipeline_handle_t pipeline);

void morse_detect_start(morse_config_t* config);
bool morse_detect_is_running();
void morse_detect_stop();

#endif
