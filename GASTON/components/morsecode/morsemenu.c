
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "esp_system.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "menu.h"
#include "common.h"
#include "lcd.h"
#include "audio_handeler.h"

#include "morsecode.h"
#include "morsemenu.h"

static const short allowedFrequencyList[] = {
    500,
    550,
    700,
    900
};

static morse_config_t morseConfig = {
    .speed = 17,
    .frequency = 550
};

static bool menuHookInput = false;
static bool settingSpeed = false;
static bool settingFrequency = false;

#define ALLOWED_FREQUENCY_LIST_LENGTH (sizeof(allowedFrequencyList) / sizeof(allowedFrequencyList[0]))

static void update_speed_line();
static void update_frequency_line();

void morse_menu_show() {
    menuHookInput = true;

    menu_draw((void*)9 /* Draw morse menu */);
    menu_changePos(1); // Select 'Speed'-row

    update_speed_line();
    update_frequency_line();

    settingSpeed = true;
}

bool morse_menu_hook_input() {
    return menuHookInput;
}

void morse_menu_exit() {
    menuHookInput = false;
    settingSpeed = false;
    settingFrequency = false;

    menu_draw((void*)0 /* go to main menu */);
}

void morse_menu_start_detect() {

    if(!morse_detect_is_running()) {
        menu_draw((void*)10 /* Draw morse screen */);

        lcd_takeMutex();

        lcd_setCursorToLine(1);
        lcd_write_string("Morse Initializing..");

        lcd_giveMutex();

        audio_handler_playAudio(AUDIO_PIPE_MORSEDETECT, &morseConfig);
    }
    
}

void morse_menu_stop_detect() {
    
    if(morse_detect_is_running()) {
        audio_handler_playAudio(AUDIO_PIPE_USEVOICE, NULL);
    }

    menu_draw((void*) 0 /* Go to main menu */);
}

void morse_menu_move(int16_t val) {

    if(settingSpeed) {
        morseConfig.speed += val;

        if(morseConfig.speed < 10)
            morseConfig.speed = 10;
        else if(morseConfig.speed > 30)
            morseConfig.speed = 30;

        update_speed_line();
    }

    if(settingFrequency) {

        // Get current selected frequency
        int i;
        for(i = 0; i < ALLOWED_FREQUENCY_LIST_LENGTH; i++) {
            if(allowedFrequencyList[i] == morseConfig.frequency)
                break;
        }

        // Get new frequency index
        i = bound_loop(i + val, 0, ALLOWED_FREQUENCY_LIST_LENGTH - 1);

        // Select frequency
        morseConfig.frequency = allowedFrequencyList[i];

        update_frequency_line();
    }

}

void morse_menu_press() {

    if(settingSpeed) {
        settingSpeed = false;
        settingFrequency = true;

        menu_changePos(1 /* Move cursor to frequency */);
    }
    else if(settingFrequency) {
        settingFrequency = false;
        menuHookInput = false;

        menu_changePos(1 /* Move cursor to apply */);
    }

}

static void update_speed_line() {
    static char lcdBuffer[6]; /* 00wpm + NULL */

    sprintf(lcdBuffer, "%02hhdwpm", morseConfig.speed);

    lcd_takeMutex();

    lcd_setCursor(8, 1);
    lcd_write_string(lcdBuffer);
    
    lcd_giveMutex();
}

static void update_frequency_line() {
    static char lcdBuffer[6]; /* 000Hz + NULL */

    sprintf(lcdBuffer, "%03hdHz", morseConfig.frequency);

    lcd_takeMutex();

    lcd_setCursor(12, 2);
    lcd_write_string(lcdBuffer);

    lcd_giveMutex();
}