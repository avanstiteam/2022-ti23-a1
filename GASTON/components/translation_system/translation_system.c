// Made by Berend de Groot.

#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "menu.h"
#include "clock.h"
#include "translation_system.h"

void process_selected_language_for_menu(MENU menus[], int selected_language) {
    switch (selected_language)
    {
    case DUTCH_LANGUAGE:
        // Home screen.
        menus[0].items[0].itemText = "Muziek";
        menus[0].items[1].itemText = "Klok";
        menus[0].items[2].itemText = "Morsecode";
        menus[0].items[3].itemText = "Talen";

        // Music screen.
        menus[1].items[0].itemText = "Terug";
        menus[1].items[1].itemText = "Radio";
        menus[1].items[2].itemText = "Bluetooth";

        // Clock screen.
        menus[2].items[0].itemText = "Terug";
        menus[2].items[1].itemText = "Luister tijd";
        menus[2].items[2].itemText = "Timer";
        menus[2].items[3].itemText = "Wekker";

        // Radio selection screen.
        menus[3].items[0].itemText = "Terug";

        // Radio screen.
        menus[4].items[0].itemText = "Terug";
        menus[4].items[1].itemText = "Selecteer zender";
        menus[4].items[2].itemText = "Huidige zender:";

        // Language screen.
        menus[5].items[0].itemText = "Terug";
        menus[5].items[3].itemText = "Volgende";

        // Language screen (2nd).
        menus[6].items[0].itemText = "Terug";


        // Morse code settings screen.
        menus[9].items[0].itemText = "Terug";
        menus[9].items[3].itemText = "Bevestig";

        // Morse code screen.
        menus[10].items[0].itemText = "Terug";
        break;
    case ENGLISH_LANGUAGE:
        // Home screen.
        menus[0].items[0].itemText = "Music";
        menus[0].items[1].itemText = "Clock";
        menus[0].items[2].itemText = "Morsecode";
        menus[0].items[3].itemText = "Languages";

        // Music screen.
        menus[1].items[0].itemText = "Return";
        menus[1].items[1].itemText = "Radio";
        menus[1].items[2].itemText = "Bluetooth";

        // Clock screen.
        menus[2].items[0].itemText = "Return";
        menus[2].items[1].itemText = "Listen to time";
        menus[2].items[2].itemText = "Timer";
        menus[2].items[3].itemText = "Alarm";

        // Radio selection screen.
        menus[3].items[0].itemText = "Return";

        // Radio screen.
        menus[4].items[0].itemText = "Return";
        menus[4].items[1].itemText = "Select station";
        menus[4].items[2].itemText = "Current:";

        // Language screen.
        menus[5].items[0].itemText = "Return";
        menus[5].items[3].itemText = "Next";

        // Language screen (2nd).
        menus[6].items[0].itemText = "Return";

        // Morse code settings screen.
        menus[9].items[0].itemText = "Return";
        menus[9].items[3].itemText = "Confirm";

        // Morse code screen.
        menus[10].items[0].itemText = "Return";
        break;
    case GERMAN_LANGUAGE:

        // NOTE: LCD does not support diaeresis sign, so we leave both that and the umlaut out.

        // Home screen.
        menus[0].items[0].itemText = "Musik";
        menus[0].items[1].itemText = "Uhr";
        menus[0].items[2].itemText = "Morsecode";
        menus[0].items[3].itemText = "Sprachen";

        // Music screen.
        menus[1].items[0].itemText = "Zuruck";
        menus[1].items[1].itemText = "Radio";
        menus[1].items[2].itemText = "Bluetooth";

        // Clock screen.
        menus[2].items[0].itemText = "Zuruck";
        menus[2].items[1].itemText = "Zeit horen";
        menus[2].items[2].itemText = "Timer";
        menus[2].items[3].itemText = "Wecker";

        // Radio selection screen.
        menus[3].items[0].itemText = "Zuruck";

        // Radio screen.
        menus[4].items[0].itemText = "Zuruck";
        menus[4].items[1].itemText = "Sender wahlen";
        menus[4].items[2].itemText = "Aktuelle:";

        // Language screen.
        menus[5].items[0].itemText = "Zuruck";
        menus[5].items[3].itemText = "Nachste";

        // Language screen (2nd).
        menus[6].items[0].itemText = "Zuruck";

        // Morse code settings screen.
        menus[9].items[0].itemText = "Zuruck";
        menus[9].items[3].itemText = "Bestätigt";

        // Morse code screen.
        menus[10].items[0].itemText = "Zuruck";
        break;
    case FRENCH_LANGUAGE:

        // NOTE: We leave out the accent aigu etc. since the LCD does not support it.

        // Home screen.
        menus[0].items[0].itemText = "La musique";
        menus[0].items[1].itemText = "L'horloge";
        menus[0].items[2].itemText = "Morse";
        menus[0].items[3].itemText = "Langues";

        // Music screen.
        menus[1].items[0].itemText = "Retourner";
        menus[1].items[1].itemText = "Radio";
        menus[1].items[2].itemText = "Bluetooth";

        // Clock screen.
        menus[2].items[0].itemText = "Retourner";
        menus[2].items[1].itemText = "Ecouter le temps";
        menus[2].items[2].itemText = "Le minuteur";
        menus[2].items[3].itemText = "L'alarme";

        // Radio selection screen.
        menus[3].items[0].itemText = "Retourner";

        // Radio screen.
        menus[4].items[0].itemText = "Retourner";
        menus[4].items[1].itemText = "Choisir chaînes de radio";
        menus[4].items[2].itemText = "Chaîne radio:";

        // Language screen.
        menus[5].items[0].itemText = "Retourner";
        menus[5].items[3].itemText = "Suivant";

        // Language screen (2nd).
        menus[6].items[0].itemText = "Retourner";

        // Morse code settings screen.
        menus[9].items[0].itemText = "Retourner";
        menus[9].items[3].itemText = "Confirmé";

        // Morse code screen.
        menus[10].items[0].itemText = "Retourner";
        break;
    default:
        printf("No valid language found: %d \n", selected_language);
        break;
    }
}

int get_selected_language()
{
    int selected_language = DUTCH_LANGUAGE; // Dutch language is the default language of our device.
    return selected_language;
}

char* get_language_path(int selected_language, char* path) {
    char *to_play = (char *) malloc(sizeof(char) * 255);

    switch (selected_language)
    {
    case ENGLISH_LANGUAGE:
        strcpy(to_play, ENGLISH_PATH);
        break;
    case DUTCH_LANGUAGE:
        strcpy(to_play, DUTCH_PATH);
        break;
    case GERMAN_LANGUAGE:
        strcpy(to_play, GERMAN_PATH);
        break;
    case FRENCH_LANGUAGE:
        strcpy(to_play, FRENCH_PATH);
        break;
    default:
        strcpy(to_play, DUTCH_PATH);
        printf("Invalid language path for language: %d", selected_language);
        printf("Default to the Dutch language.");
        break;
    }

    strcat(to_play, path);
    return to_play;
}

/**
 * @brief Calls all other methods that handle translations.
 * 
 * @param selected_language language to change to.
 */
static void set_preferred_language(int selected_language) {
    process_selected_language_menu(selected_language);
    set_selected_language_clock(selected_language);
    printf("Language set to: %d \n", selected_language);
}

void set_language_to_english() {
    set_preferred_language(ENGLISH_LANGUAGE);
}

void set_language_to_dutch() {
    set_preferred_language(DUTCH_LANGUAGE);
}

void set_language_to_german() {
    set_preferred_language(GERMAN_LANGUAGE);
}

void set_language_to_french() {
    set_preferred_language(FRENCH_LANGUAGE);
}