#include "menu.h"

// Language values.
#define ENGLISH_LANGUAGE 0
#define DUTCH_LANGUAGE 1
#define GERMAN_LANGUAGE 2
#define FRENCH_LANGUAGE 3

// Language filePaths.
#define ENGLISH_PATH "en/"
#define DUTCH_PATH "nl/"
#define GERMAN_PATH "de/"
#define FRENCH_PATH "fr/"

void process_selected_language_for_menu(MENU menus[], int selected_language);

/**
 * @brief Returns the correct language at start up. Since we do not have this it will always return the default language (Dutch).
 * @return int Language to init with.
 */
int get_selected_language();

/**
 * @brief Returns the filepath including the correct language folder.
 * 
 * @param selected_language The language to retrieve the file for.
 * @param path File available in all languages
 * @return char* Filepath to find localized file at.
 */
char* get_language_path(int selected_language, char* path);

/**
 * @brief Changes the language to English.
 */
void set_language_to_english();

/**
 * @brief Changes the language to Dutch.
 */
void set_language_to_dutch();

/**
 * @brief Changes the language to German.
 */
void set_language_to_german();

/**
 * @brief Changes the language to French.
 */
void set_language_to_french();