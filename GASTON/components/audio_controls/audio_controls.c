#include <stdio.h>
#include "extgpio.h"
#include "custom_button.h"

#include "esp_log.h"

#define GPIO GPIOA

#define SKIP_PIN 1
#define PAUSE_PIN 0
#define VOL_UP_PIN 3
#define VOL_DWON_PIN 2

static button_handle_t skipButton;
static button_handle_t pauseButton;
static button_handle_t volumeUpButton;
static button_handle_t volumeDownButton;

static void (*skipButtonCallback)(void);
static void (*pauseButtonCallback)(void);
static void (*volumeButtonCallback)(int);

static const char* LOG_TAG = "AUDIO_MENU_CONTROLLER";

static void skipButtonClick(button_handle_t btn) {
    skipButtonCallback();
}

static void pauseButtonClick(button_handle_t btn) {
    pauseButtonCallback();
}

static void volumeUpButtonClick(button_handle_t btn) {
    volumeButtonCallback(5);
}

static void volumeDownButtonClick(button_handle_t btn) {
    volumeButtonCallback(-5);
}

void button_audio_controller_init() {
    skipButton = btn_create(GPIO, extgpio_pin(SKIP_PIN));
    pauseButton = btn_create(GPIO, extgpio_pin(PAUSE_PIN));
    volumeUpButton = btn_create(GPIO, extgpio_pin(VOL_UP_PIN));
    volumeDownButton = btn_create(GPIO, extgpio_pin(VOL_DWON_PIN));

    btn_register_click_handler(skipButton, skipButtonClick);
    btn_register_click_handler(pauseButton, pauseButtonClick);
    btn_register_click_handler(volumeUpButton, volumeUpButtonClick);
    btn_register_click_handler(volumeDownButton, volumeDownButtonClick);

    ESP_LOGI(LOG_TAG, "Initialised audio controller");
}

void audio_menu_set_volume(void (*onVolume)(int)) {
    volumeButtonCallback = onVolume;
}

void audio_menu_pause(void (*onPause)(void)) {
    pauseButtonCallback = onPause;
}

void audio_menu_skip(void (*onSkip)(void)) {
    skipButtonCallback = onSkip;
}
