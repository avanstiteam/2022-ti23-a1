#ifndef AMPLITUDE_PROCESSING
/**
 * @brief This method is used to process raw audio data and convert it to decibels.
 * 
 * @param samples Is a int16_t pointer of the measured samples.
 * @param numSamples Is the amount of samples in the samples pointer.
 * @return double Is the processed data in form of decibel.
 */
    double amplitude_processing_process(int16_t *samples, int numSamples);
#endif


