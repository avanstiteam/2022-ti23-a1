#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "amplitude-processing.h"

/**
 * @brief This method is used to calculate the root mean squared of the raw data.
 * Take all the measurements and put them to the power of 2.
 * Then divide the sum with the length of the samples.
 * This whill give you the mean.
 * Lastly Square root it to get the root mean square.
 * Calculate the squared.
 * @param samples is an int16_t pointer of the samples.
 * @param numSamples is the amount of samples present in samples.
 * @return double. The calculated rms value.
 */
static inline double amplitude_processing_rms(int16_t *samples, int numSamples) {

    double sum = 0;

    for(int s = 0; s < numSamples; s++) {
        sum += pow(abs(samples[s]), 2);
    }
    return (sqrt(sum / numSamples)) / sqrt(2);
}

/**
 * @brief This method is used to calculate the decibels of the audio.
 * 
 * @param rsm is a value calculated with rsm.
 * @return double. The calculated decibels. 
 */
static inline double amplitude_processing_dec(double rsm) {
    return 20 * log10(rsm);
}


double amplitude_processing_process(int16_t *samples, int numSamples)
{
    double rms = amplitude_processing_rms(samples, numSamples);
    return amplitude_processing_dec(rms);
}


