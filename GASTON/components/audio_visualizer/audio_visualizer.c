#include <stdio.h>
#include "audio_visualizer.h"
#include "extgpio.h"

#define AUDIO_VISUALIZER_MIN_DECIBEL 50
#define AUDIO_VISUALIZER_MAX_DECIBEL 70
#define AUDIO_VISUALIZER_TARGET_MIN 0
#define AUDIO_VISUALIZER_TARGET_MAX 8

void audio_visualizer_init(void)
{
    // Set the B row to output.
    extgpio_set_direction(GPIOB, 1);
    // Turn of all pins.
    extgpio_set(GPIOB, 0x00);
}

void audio_visualizer_write(double decibels) {
    // Calculate a scaled value between AUDIO_VISUALIZER_TARGET_MIN and AUDIO_VISUALIZER_TARGET_MAX.
    if(decibels > AUDIO_VISUALIZER_MAX_DECIBEL) decibels = AUDIO_VISUALIZER_MAX_DECIBEL;
    if(decibels < AUDIO_VISUALIZER_MIN_DECIBEL) decibels = AUDIO_VISUALIZER_MIN_DECIBEL;
    decibels = 
    ((decibels - AUDIO_VISUALIZER_MIN_DECIBEL) / 
    (AUDIO_VISUALIZER_MAX_DECIBEL - AUDIO_VISUALIZER_MIN_DECIBEL)) *
    (AUDIO_VISUALIZER_TARGET_MAX - AUDIO_VISUALIZER_TARGET_MIN) + AUDIO_VISUALIZER_TARGET_MIN;

    int scaledValue = (int) decibels;

    char pinValue = 0;
    // Start at zero and for i < scaledValue shift the pinValue one to the left and set the first bit to one.
    // Doing this will set the value each pin. So a scaled value of 3 would result in 0b00000111.
    for(int i = 0; i < scaledValue; i++) {
        pinValue = pinValue << 1;
        pinValue |= 1;
    }
    extgpio_set(GPIOB, pinValue);

}
