/**
 * @brief This method is used to initialize the audio visualizer.
 * 
 */
void audio_visualizer_init(void);

/**
 * @brief This method is used to write to the output of the audio visualizer.
 * 
 * @param decibels The amount of decibels measured by the audio recognizer.
 */
void audio_visualizer_write(double decibels);
