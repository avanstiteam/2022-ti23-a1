#ifndef LYRAT_SCK
#define LYRAT_SCK 23
#endif
#ifndef LYRAT_SDA
#define LYRAT_SDA 18
#endif

#define LCD_NUM_ROWS               4
#define LCD_NUM_COLUMNS            16
#define LCD_NUM_VISIBLE_COLUMNS    20

#define I2C_MASTER_NUM           I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled
#define I2C_MASTER_FREQ_HZ       100000
#define I2C_MASTER_SDA_IO        LYRAT_SDA
#define I2C_MASTER_SCL_IO        LYRAT_SCK
#ifndef LYRAT_SCK_SPEED
#define LYRAT_SCK_SPEED 10000
#endif

#ifndef LCD_I2C_ADDR
#define LCD_I2C_ADDR 0x27
#endif

#include "driver/i2c.h"

i2c_config_t i2c_init();