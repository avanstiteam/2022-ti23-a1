///////////////////////////
// Rewriting audio code to fit in
// one module.
// Thomas and Jasper
///////////////////////////
#include <stdio.h>
#include <string.h>
#include "audio_handeler.h"

#include "audio_pipeline.h"
#include "http_stream.h"
#include "i2s_stream.h"
#include "aac_decoder.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "fatfs_stream.h"
#include "mp3_decoder.h"
#include "raw_stream.h"
#include "filter_resample.h"
#include "periph_sdcard.h"
#include "board.h"
#include "audio_init.h"
#include "audio_recognition.h"
#include "morsecode.h"
#include "radio.h"
#include "lcd.h"

#include "esp_log.h"

#ifndef AUDIO_SAMPLE_RATE
#define AUDIO_SAMPLE_RATE 48000 
#endif

static const char *TAG = "Audio_Handler";

audio_pipeline_handle_t pipeline;
audio_type currentType;
audio_event_iface_handle_t evt;
esp_periph_set_handle_t set;

const char *radio_link_tag[3] = {"http", "aac", "i2s"};
const char *sd_link_tag[3] = {"file", "dec", "i2s"};

char radio_isRunning = 0;
static bool radio_isOn = false;
static bool radio_is_mute = 0;
static bool voice_is_running = 0;
static TaskHandle_t audio_handle;

// Radio

// SD
static char *FILE_PREFIX = "file://sdcard/";
static char *FILE_SUFFIX = ".mp3";

audio_element_handle_t http_stream_reader,
                        aac_decoder, 
                        i2s_stream_writer,
                        fatfs_stream_reader,
                        music_decoder,
                        i2s_stream_reader,
                        resample_filter,
                        raw_reader;


static int _http_stream_event_handle(http_stream_event_msg_t *msg)
{
    if (msg->event_id == HTTP_STREAM_RESOLVE_ALL_TRACKS) {
        return ESP_OK;
    }

    if (msg->event_id == HTTP_STREAM_FINISH_TRACK) {
        return http_stream_next_track(msg->el);
    }
    if (msg->event_id == HTTP_STREAM_FINISH_PLAYLIST) {
        return http_stream_fetch_again(msg->el);
    }
    return ESP_OK;
}

static void set_audio(char *file_to_play) {
    char *to_play = (char *) malloc(sizeof(char) * 255);
    
    strcpy(to_play, FILE_PREFIX);
    strcat(to_play, file_to_play);
    strcat(to_play, FILE_SUFFIX);

    ESP_LOGI(TAG, "PRINTING OUT FILENAME %s", to_play);
    audio_element_set_uri(fatfs_stream_reader, to_play);
    free(to_play);
}

void audio_handler_startRecognition() {
    audio_recognition_start();
    voice_is_running = true;
}
void audio_handler_stopRecognition() {
    ESP_LOGI(TAG, "Stopping audio recognition");
    audio_recognition_stop();
    voice_is_running = false;
}

void audio_handler_pipeline_init(void)
{
    ESP_LOGI(TAG, "Create audio pipeline for playback");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);
    mem_assert(pipeline);

    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    set = esp_periph_set_init(&periph_cfg);

    ESP_LOGI(TAG, "Create fatfs stream to read data from sdcard");
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);

    ESP_LOGI(TAG, "Create i2s stream to write data to codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_stream_writer = i2s_stream_init(&i2s_cfg);

    ESP_LOGI(TAG, "Create mp3 decoder");
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    music_decoder = mp3_decoder_init(&mp3_cfg);

    ESP_LOGI(TAG, "Create http stream to read data");
    http_stream_cfg_t http_cfg = HTTP_STREAM_CFG_DEFAULT();
    http_cfg.event_handle = _http_stream_event_handle;
    http_cfg.type = AUDIO_STREAM_READER;
    http_cfg.enable_playlist_parser = true;
    http_stream_reader = http_stream_init(&http_cfg);

    ESP_LOGI(TAG, "Create aac decoder to decode aac file");
    aac_decoder_cfg_t aac_cfg = DEFAULT_AAC_DECODER_CONFIG();
    aac_decoder = aac_decoder_init(&aac_cfg);

    ESP_LOGI(TAG, "Create i2s stream to read data to codec chip");
    i2s_stream_cfg_t i2s_cfg_r = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg_r.type = AUDIO_STREAM_READER;
    i2s_cfg_r.i2s_config.sample_rate = AUDIO_SAMPLE_RATE;
    i2s_stream_reader = i2s_stream_init(&i2s_cfg_r);

    ESP_LOGI(TAG, "Create resample filter");
    rsp_filter_cfg_t rsp_cfg = DEFAULT_RESAMPLE_FILTER_CONFIG();
    rsp_cfg.src_rate = AUDIO_SAMPLE_RATE;
    rsp_cfg.src_ch = 2;
    rsp_cfg.dest_rate = 8000;
    rsp_cfg.dest_ch = 1;
    resample_filter = rsp_filter_init(&rsp_cfg);

    ESP_LOGI(TAG, "Create raw reader for audio processing");
    raw_stream_cfg_t raw_cfg = {
        .out_rb_size = 8 * 1024,
        .type = AUDIO_STREAM_READER,
    };
    raw_reader = raw_stream_init(&raw_cfg);

    audio_board_sdcard_init(set, SD_MODE_1_LINE);
    audio_recognition_init(&pipeline);
    morse_init(pipeline);
}

audio_element_handle_t audio_handler_get_i2s_stream_reader() {
    return i2s_stream_reader;
}

audio_element_handle_t audio_handler_get_resample_filter() {
    return resample_filter;
}

audio_element_handle_t audio_handler_get_raw_reader() {
    return raw_reader;
}

static void audio_handler_switchMode(audio_type type) {
    ESP_LOGI(TAG, "Current tag is: %d", currentType);
    ESP_LOGI(TAG, "New tag is: %d", type);
    ESP_LOGI(TAG, "Voice is: %d", voice_is_running);
    //if(type == currentType) return;
    switch (currentType) {
        case AUDIO_PIPE_USEVOICE:
            if(voice_is_running) {
                audio_handler_stopRecognition();
            }
            break;
        case AUDIO_PIPE_USERADIO:
            if(radio_isOn) {
                printf("here\n");
                audio_handler_stopAudio();
            }
            break;
        case AUDIO_PIPE_MORSEDETECT:
            if(morse_detect_is_running()) {
                morse_detect_stop();

                vTaskDelay(200 / portTICK_RATE_MS); // Wait for morsedetect to stop
            }
            break;
    }

    switch (type)
    {
    case AUDIO_PIPE_USERADIO:
        currentType = AUDIO_PIPE_USERADIO;
        break;
    case AUDIO_PIPE_USESD:
        currentType = AUDIO_PIPE_USESD;
        break;
    case AUDIO_PIPE_USEVOICE:
        currentType = AUDIO_PIPE_USEVOICE;
        break;
    case AUDIO_PIPE_MORSEDETECT:
        currentType = AUDIO_PIPE_MORSEDETECT;
        break;
    default:
        ESP_LOGI(TAG, "Audiotype %d is not a valid audiotype!", type);
        break;
    }
}

static void audio_handler_playRadio(void * parameter) {
    char * url = (char *)parameter;
    radio_isOn = true;
    ESP_LOGI(TAG, "[2.4] Register all elements to audio pipeline");
    audio_pipeline_register(pipeline, http_stream_reader, "http");
    audio_pipeline_register(pipeline, aac_decoder,        "aac");
    audio_pipeline_register(pipeline, i2s_stream_writer,  "i2s");

    ESP_LOGI(TAG, "[2.5] Link it together http_stream-->aac_decoder-->i2s_stream-->[codec_chip]");
    audio_pipeline_link(pipeline, &radio_link_tag[0], 3);

    ESP_LOGI(TAG, "[2.6] Set up  uri (http as http_stream, aac as aac decoder, and default output is i2s)");
    audio_element_set_uri(http_stream_reader, url);

    //WIFI GOES HERE!!
    ESP_LOGI(TAG, "[ 3 ] Start and wait for Wi-Fi network");
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    set = esp_periph_set_init(&periph_cfg);
    // periph_wifi_cfg_t wifi_cfg = {
    //     .ssid = CONFIG_WIFI_SSID,
    //     .password = CONFIG_WIFI_PASSWORD,
    // };
    // esp_periph_handle_t wifi_handle = periph_wifi_init(&wifi_cfg);
    // esp_periph_start(set, wifi_handle);
    // periph_wifi_wait_for_connected(wifi_handle, portMAX_DELAY);
    

    ESP_LOGI(TAG, "[ 4 ] Set up  event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);

    ESP_LOGI(TAG, "[4.1] Listening event from all elements of pipeline");
    audio_pipeline_set_listener(pipeline, evt);

    ESP_LOGI(TAG, "[4.2] Listening event from peripherals");
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);

    ESP_LOGI(TAG, "[ 5 ] Start audio_pipeline");
    audio_pipeline_run(pipeline);
    // I do not know why the radio loop needs to be looped seven times to have a normal play speed but it works.
    radio_isRunning = 7;

    lcd_takeMutex();

    lcd_setCursor(2, 3);
    lcd_write_string(get_station_name());

    lcd_giveMutex();

    while (radio_isRunning) {

        printf("Radio running: %d\n", radio_isRunning);
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK) {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT
            && msg.source == (void *) aac_decoder
            && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO) {
            audio_element_info_t music_info = {0};
            ESP_LOGE(TAG, "[ * ] Getting info");
            audio_element_getinfo(aac_decoder, &music_info);

            ESP_LOGI(TAG, "[ * ] Receive music info from aac decoder, sample_rates=%d, bits=%d, ch=%d",
                     music_info.sample_rates, music_info.bits, music_info.channels);

            audio_element_setinfo(i2s_stream_writer, &music_info);
            i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
            continue;
        }

        /* restart stream when the first pipeline element (http_stream_reader in this case) receives stop event (caused by reading errors) */
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *) http_stream_reader
            && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (int) msg.data == AEL_STATUS_ERROR_OPEN) {
            ESP_LOGW(TAG, "[ * ] Restart stream");
            audio_pipeline_stop(pipeline);
            audio_pipeline_wait_for_stop(pipeline);
            audio_element_reset_state(aac_decoder);
            audio_element_reset_state(i2s_stream_writer);
            audio_pipeline_reset_ringbuffer(pipeline);
            audio_pipeline_reset_items_state(pipeline);
            audio_pipeline_run(pipeline);
            continue;
        }
        radio_isRunning--;
    
    }
    vTaskDelete(NULL);
}

static void audio_handler_playSD(void * parameter) {
    char * fileName = (void *) parameter;
    // Initialize SD Card peripheral
    // audio_board_sdcard_init(set, SD_MODE_1_LINE);
    ESP_LOGI(TAG, "[3.4] Register all elements to audio pipeline");
    audio_pipeline_register(pipeline, fatfs_stream_reader, "file");
    audio_pipeline_register(pipeline, music_decoder, "dec");
    audio_pipeline_register(pipeline, i2s_stream_writer, "i2s");

    ESP_LOGI(TAG, "[3.5] Link it together [sdcard]-->fatfs_stream-->music_decoder-->i2s_stream-->[codec_chip]");
    const char *link_tag[3] = {"file", "dec", "i2s"};
    audio_pipeline_link(pipeline, &link_tag[0], 3);

    ESP_LOGI(TAG, "[3.6] Set up uri: /sdcard/%s.mp3", fileName);
    audio_element_set_uri(fatfs_stream_reader, "/sdcard/test.mp3");

     ESP_LOGI(TAG, "[ 4 ] Set up  event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    audio_event_iface_handle_t evt = audio_event_iface_init(&evt_cfg);
    ESP_LOGI(TAG, "[4.1] Listening event from all elements of pipeline");
    audio_pipeline_set_listener(pipeline, evt);
    ESP_LOGI(TAG, "[4.2] Listening event from peripherals");
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);
    

    // ESP_LOGI(TAG, "[4.1] Listening event from all elements of pipeline");
    // audio_pipeline_set_listener(pipeline, evt);

    // ESP_LOGI(TAG, "[4.2] Listening event from peripherals");
    // audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);

    ESP_LOGI(TAG, "[ 5 ] Start audio_pipeline");
    set_audio(fileName);
    audio_pipeline_run(pipeline);


    ESP_LOGI(TAG, "[ 6 ] Listen for all pipeline events");
    while (1) {
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);

        if (ret != ESP_OK) {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *) music_decoder
            && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO) {
            audio_element_info_t music_info = {0};
            audio_element_getinfo(music_decoder, &music_info);

            ESP_LOGI(TAG, "[ * ] Receive music info from mp3 decoder, sample_rates=%d, bits=%d, ch=%d",
                     music_info.sample_rates, music_info.bits, music_info.channels);

            audio_element_setinfo(i2s_stream_writer, &music_info);
            i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
            continue;
        }

        /* Stop when the last pipeline element (i2s_stream_writer in this case) receives stop event */
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *) i2s_stream_writer
            && msg.cmd == AEL_MSG_CMD_REPORT_STATUS
            && (((int)msg.data == AEL_STATUS_STATE_STOPPED) || ((int)msg.data == AEL_STATUS_STATE_FINISHED))) {
            ESP_LOGW(TAG, "[ * ] Stop event received");
            break;
        }
    }
    ESP_LOGI(TAG, "[ 7 ] stopping audio_pipeline");
    
    audio_pipeline_stop(pipeline);
    audio_pipeline_wait_for_stop(pipeline);
    audio_pipeline_terminate(pipeline);

    audio_pipeline_unregister(pipeline, fatfs_stream_reader);
    audio_pipeline_unregister(pipeline, i2s_stream_writer);
    audio_pipeline_unregister(pipeline, music_decoder);

    /* Terminal the pipeline before removing the listener */
    ESP_LOGI(TAG, "[ 7.3] Removing listeners");
    audio_pipeline_remove_listener(pipeline);

    /* Stop all periph before removing the listener */
    esp_periph_set_stop_all(set);
    audio_event_iface_remove_listener(esp_periph_set_get_event_iface(set), evt);

    /* Make sure audio_pipeline_remove_listener & audio_event_iface_remove_listener are called before destroying event_iface */
    audio_event_iface_destroy(evt);

}

static void audio_handler_playRecognition() {
    audio_handler_startRecognition();
}

void audio_handler_playAudio(audio_type type, void* data) {
    
    // if(currentType == AUDIO_PIPE_USEVOICE) {
    //     if(voice_is_running) {
    //         audio_handler_stopRecognition();
    //     }
    // }
    
    audio_handler_switchMode(type);
    switch (type)
    {
    case AUDIO_PIPE_USERADIO:
    radio_isRunning = 7;
        xTaskCreatePinnedToCore(audio_handler_playRadio, "Audio task", 1024 * 4, data, 6, &audio_handle, 1);
        break;
    case AUDIO_PIPE_USESD:
        printf("playing sd\n");
        audio_handler_playSD((char*)data);
        break;
    case AUDIO_PIPE_USEVOICE:
        audio_handler_playRecognition();
        break;
    case AUDIO_PIPE_MORSEDETECT:
        morse_detect_start((morse_config_t*) data);
    default:
        break;
    }
}

void audio_handler_stopAudio(void) {
   // vTaskDelete(audio_handle);
    audio_pipeline_stop(pipeline);
    audio_pipeline_wait_for_stop(pipeline);
    audio_pipeline_terminate(pipeline);
    switch(currentType) {
        case AUDIO_PIPE_USERADIO:
            ESP_LOGI(TAG, "[ 6.2 ] Unregistering pipeline");
            audio_pipeline_unregister(pipeline, http_stream_reader);
            audio_pipeline_unregister(pipeline, aac_decoder);
           // audio_pipeline_unregister(pipeline, i2s_stream_writer);
            break;
        case AUDIO_PIPE_USESD:
            ESP_LOGI(TAG, "[ 6.2 ] Unregistering pipeline");
            audio_pipeline_unregister(pipeline, fatfs_stream_reader);
           // audio_pipeline_unregister(pipeline, i2s_stream_writer);
            audio_pipeline_unregister(pipeline, music_decoder);
        // case AUDIO_PIPE_USEVOICE:
        //     audio_pipeline_register(pipeline, resample_filter, "rsp_filter");
        //     audio_pipeline_register(pipeline, raw_reader, "raw");
        //     break;    
    }
    audio_pipeline_unregister(pipeline, i2s_stream_writer);
    /* Terminate the pipeline before removing the listener */
    ESP_LOGI(TAG, "[ 6.3 ] Removing listeners");
    audio_pipeline_remove_listener(pipeline);

    /* Stop all peripherals before removing the listener */
    ESP_LOGI(TAG, "[ 6.4 ] Stopping set");
    esp_periph_set_stop_all(set);
    audio_event_iface_remove_listener(esp_periph_set_get_event_iface(set), evt);

    /* Make sure audio_pipeline_remove_listener & audio_event_iface_remove_listener are called before destroying event_iface */
    ESP_LOGI(TAG, "[ 6.5 ] Destroying iface");
    audio_event_iface_destroy(evt);
    radio_isOn = false;
}

int mute = 0;

void audio_handler_muteRadio() {
    //ADD RADIO CHECK
    if (pipeline != NULL) {
        if (mute == 0)
        {
            mute = 1;
            audio_hal_set_mute(audio_board_handle->audio_hal, 1);
        } else if (mute == 1)
        {
            mute = 0;
            audio_hal_set_mute(audio_board_handle->audio_hal, 0);
        }
    }
}