#pragma once

#define AUDIO_PIPE_NON 0
#define AUDIO_PIPE_USERADIO 1
#define AUDIO_PIPE_USESD 2
#define AUDIO_PIPE_USEVOICE 3
#define AUDIO_PIPE_MORSEDETECT 4

#include "audio_pipeline.h"

typedef int audio_type;
extern audio_pipeline_handle_t pipeline;
extern char radio_isRunning;

void audio_handler_pipeline_init(void);
/**
 * @brief Can be used to start playing audio. The pipeline needs to be initialized before this call.
 * 
 * @param type Is an audio_type of which you want to play.
 * @param data Pointer to extra data for the player
 * 
 * Radio starts it's own task.
 * SD is a blocking call. This should be called from an external task.
 */
void audio_handler_playAudio(audio_type type, void* data);
void audio_handler_stopAudio(void);
void audio_handler_muteRadio();
void audio_handler_startRecognition();
void audio_handler_stopRecognition();

audio_element_handle_t audio_handler_get_i2s_stream_reader();
audio_element_handle_t audio_handler_get_resample_filter();
audio_element_handle_t audio_handler_get_raw_reader();