#ifndef RADIO_H
#define RADIO_H
typedef struct {
    char* stationName;
    char* link;
} STATION;

extern STATION stations[];

extern char* AAC_STREAM_URI;
extern int currentStation;

void radio_init();
void radio_start();
void radio_stop();
void radio_changeStation(int i);
void radio_skipStation(int channel);

void radio_changeVolume(int);
void muteRadio();

char* get_station_name();
#endif