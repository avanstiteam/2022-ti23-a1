/* Play M3U HTTP Living stream

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "sdkconfig.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "http_stream.h"
#include "i2s_stream.h"
#include "aac_decoder.h"
#include "esp_peripherals.h"
#include "periph_wifi.h"
#include "board.h"

#include "radio.h"
#include "lcd.h"
#include "TouchButtons.h"
#include "audio_init.h"
#include "esp_log.h"
//#include "audio_handeler.h"


#if __has_include("esp_idf_version.h")
#include "esp_idf_version.h"
#else
#define ESP_IDF_VERSION_VAL(major, minor, patch) 1
#endif

#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4, 1, 0))
#include "esp_netif.h"
#else
#include "tcpip_adapter.h"
#endif

int VOLUME = AUDIO_HAL_VOL_DEFAULT;

static const char *TAG = "Radio";

char* AAC_STREAM_URI = "http://stream.radiocorp.nl/web14_aac";
int currentStation = 0;
char radio_isOn = 0;
int radio_is_mute = 0;

STATION stations[] = {
    {
        "Radio 538",
        "http://playerservices.streamtheworld.com/api/livestream-redirect/RADIO538AAC.aac"
    },
    {
        "Slam",
        "http://stream.radiocorp.nl/web14_aac"
    },
    {
        "SkyRadio",
        "http://playerservices.streamtheworld.com/api/livestream-redirect/SKYRADIOAAC.aac"
    }
};

void radio_run(void * parameter);

// void radio_start() {
//     audio_handler_playAudio(AUDIO_PIPE_USERADIO, AAC_STREAM_URI);
//     //xTaskCreatePinnedToCore(radio_startTask, "Radio Start", 1024 * 4, NULL, 6, &startHandle, 0);
// }

// void radio_stop() {
//     audio_handler_stopAudio();
// }

void radio_changeStation(int channel) {
    currentStation = channel;
    AAC_STREAM_URI = stations[currentStation].link;

    //audio_handler_stopAudio();
    //audio_handler_playAudio(AUDIO_PIPE_USERADIO, AAC_STREAM_URI);
}

void radio_skipStation(int channel) {
    currentStation += channel;

    if (currentStation == (sizeof(stations) / sizeof(stations[0])))
    {
        currentStation = 0;
    }
    

    AAC_STREAM_URI = stations[currentStation].link;
}

void radio_changeVolume(int vol) {
    VOLUME += vol;

    if (VOLUME > 100)
    {
        VOLUME = 100;
    } else if (VOLUME < 0) {
        VOLUME = 0;
    }

    audio_hal_set_volume(audio_board_handle->audio_hal, VOLUME);
    //ESP_LOGI(TAG, "Volume set to: %d \n", VOLUME);
    ESP_LOGI(TAG, "Changed radio station");
}

char* get_station_name() {
    return stations[currentStation].stationName;
}

// void muteRadio() {
//     audio_handler_muteRadio();
// }