#ifndef TIMER_CLOCK_H
#define TIMER_CLOCK_H

/**
 * @brief Method which sets the time the timer should count down from
 * 
 * @param hour The value the minutes the timer needs to be set to
 * @param minute The value the seconds the timer needs to be set to
 */
void set_timer_time(int hour, int minute);

/**
 * @brief Method which changes the state of the timer, enabled or disabled
 * 
 * @param state The value the state of the timer will be set to, 1(enabled) or 0(disabled)
 */
void set_timer_state(int state);

/**
 * @brief Method which returns the current state of the timer, enabled or disabled
 */
int get_timer_state();

/**
 * @brief Method which decrements the seconds and minutes on the timer,
 * and checks of they both are 0. If both of them are 0, the ringtone
 * will play.
 */
void check_timer_time();

/**
 * @brief Method which returns if the timer is ringing
 */
int return_timer_went_off();

#endif