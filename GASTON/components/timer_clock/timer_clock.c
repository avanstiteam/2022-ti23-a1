#include <stdio.h>

#include "radio.h"
#include "audio_handeler.h"
#include "lcd.h"
#include "alarm_clock.h"

int timer_active = 0;
int timer_went_off = 0;

typedef struct time {
    int minute;
    int second;
} TIMER_STRUCT;

TIMER_STRUCT timer_time;
uint8_t hourglass[] = {0b11111, 0b10001, 0b01010, 0b00100, 0b01010, 0b10001, 0b11111, 0b000000};

void set_timer_time(int minute, int second) {
    timer_time.minute = minute;
    timer_time.second = second;

    timer_active = 1;
}

void set_timer_state(int state) {
    timer_active = state;
}

int get_timer_state() {
    return timer_active;
}

/**
 * @brief Method which updates the timer time on every screen
 */
void update_timer_text() {

    lcd_defince_custom_character(I2C_LCD1602_INDEX_CUSTOM_1, hourglass);
        
    if (timer_active)
    {
        lcd_takeMutex();

        char timerString[80];

        sprintf(timerString, "%02d:%02d", timer_time.minute, timer_time.second);
        lcd_setCursor(14, 2);
        lcd_write_char(I2C_LCD1602_INDEX_CUSTOM_1);
        lcd_setCursor(15, 2);
        lcd_write_string(timerString);

        lcd_giveMutex();
    }
}

void check_timer_time() {

    timer_time.second--;

    if (timer_time.second < 0)
    {
        timer_time.second = 59;
        timer_time.minute--;
    }
    
    update_timer_text();

    if (timer_time.minute == 0 && timer_time.second == 0)
    {
        audio_handler_playAudio(AUDIO_PIPE_USERADIO, AAC_STREAM_URI);
        timer_active = 0;

        lcd_takeMutex();

        lcd_clear_section(14, 19, 2);

        lcd_giveMutex();

        timer_went_off = 1;
    }
}

int return_timer_went_off() {
    return timer_went_off;
}