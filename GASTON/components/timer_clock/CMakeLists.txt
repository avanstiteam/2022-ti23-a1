idf_component_register(SRCS "timer_clock.c"
                    INCLUDE_DIRS "include"
                    REQUIRES extgpio
                    REQUIRES custom_button
                    REQUIRES radio
                    REQUIRES alarm_clock)
