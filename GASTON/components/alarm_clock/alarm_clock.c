#include <stdio.h>

#include "radio.h"
#include "audio_handeler.h"
#include "lcd.h"
#include "timer_clock.h"

int alarm_active = 0;
int alarm_went_off = 0;

typedef struct time {
    int hour;
    int minute;
} TIME_STRUCT;

TIME_STRUCT alarm_time;
uint8_t alarmClock[] = {0b11011, 0b10001, 0b01110, 0b10101, 0b10101, 0b10001, 0b01110, 0b000000};

void set_alarm_time(int hour, int minute) {
    alarm_time.hour = hour;
    alarm_time.minute = minute;

    alarm_active = 1;
}

void set_alarm_state(int state) {
    alarm_active = state;
}

int get_alarm_state() {
    return alarm_active;
}

void drawTime() {
    lcd_defince_custom_character(I2C_LCD1602_INDEX_CUSTOM_2, alarmClock);   

    lcd_takeMutex();

    char timerString[80];

    sprintf(timerString, "%02d:%02d", alarm_time.hour, alarm_time.minute);
    lcd_setCursor(14, 3);
    lcd_write_char(I2C_LCD1602_INDEX_CUSTOM_2);
    lcd_setCursor(15, 3);
    lcd_write_string(timerString);

    lcd_giveMutex();
}

void check_alarm_time(int hour, int minute) {
    if (alarm_time.hour == hour && alarm_time.minute == minute)
    {
        audio_handler_playAudio(AUDIO_PIPE_USERADIO, AAC_STREAM_URI);
        alarm_active = 0;

        lcd_takeMutex();

        lcd_clear_section(14, 19, 3);

        lcd_giveMutex();

        alarm_went_off = 1;
    }

    drawTime();
}

int return_alarm_went_off() {
    return alarm_went_off;
}