#ifndef ALARM_CLOCK_H
#define ALARM_CLOCK_H

/**
 * @brief Method which sets the time of the alarm to the 
 *        given time in the parameters
 * 
 * @param hour Is the amount used to set the hours
 * @param minute Is the amount used to set the minutes
 */
void set_alarm_time(int hour, int minute);

/**
 * @brief Method which sets the alarm state to enabled or
 *        disabled
 * 
 * @param state Is the state the alarm is set to, 1 or 0
 */
void set_alarm_state(int state);

/**
 * @brief Method which returns the current state of the
 *        alarm. Is either 1 or 0
 */
int get_alarm_state();

/**
 * @brief Method which checks if the given time is equal to
 *        the time of the parameters
 * 
 * @param hour Is the amount the hours needs to be compared to
 * @param minute Is the amount the minutes needs to be compared to
 */
void check_alarm_time(int hour, int minute);

/**
 * @brief Method which draws the time of the alarm in the bottom
 *        left corner of the screen with the time and clock icon.
 */
void drawTime();

/**
 * @brief Method which returns if the alarm is ringing or not.
 *        Is either 1 or 0;
 */
int return_alarm_went_off();

#endif