idf_component_register(SRCS "extgpio.c"
                    INCLUDE_DIRS "include"
                    REQUIRES mcp23017 common
                    REQUIRES esp_peripherals)
