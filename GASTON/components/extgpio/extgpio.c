
#include <stdio.h>
#include <stdbool.h>

#include "esp_log.h"
#include "mcp23017.h"
#include "i2c_bus.h"

#include "common.h"

#include "extgpio.h"

static const char* LOG_TAG = "EXTGPIO";

static mcp23017_handle_t mcp_dev;

static SemaphoreHandle_t update_mutex;
static SemaphoreHandle_t write_mutex;
static TaskHandle_t update_task_handle;

static uint8_t gpio_in_value_list[] = { 0x00, 0x00 };
static uint8_t gpio_out_value_list[] = { 0x00, 0x00 };

static uint8_t gpio_direction_list[] = { 0x00, 0x00 };

static void extgpio_update_task(void* params);

void extgpio_init() {

    if(mcp_dev != NULL) {
        ESP_LOGW(LOG_TAG, "External GPIO is already initialized, skipping init function...");
        return;
    }

    mcp_dev = mcp23017_create(MCP23017_I2C_ADDRESS_DEFAULT);

    if(mcp_dev == NULL) {
        ESP_LOGE(LOG_TAG, "Could not create mcp23017 device at address 0x%02X", MCP23017_I2C_ADDRESS_DEFAULT);
        return;
    }

    esp_err_t err = mcp23017_check_present(mcp_dev);
    if(err != ESP_OK) {
        ESP_LOGE(LOG_TAG, "Could not create mcp23017 device at address 0x%02X, device not found!", MCP23017_I2C_ADDRESS_DEFAULT);
        mcp23017_delete(mcp_dev);
        mcp_dev = NULL;
        return;
    }

    update_mutex = xSemaphoreCreateMutex();
    write_mutex = i2c_mutex;
    xSemaphoreGive(update_mutex);

    extgpio_set_direction(GPIOA, 0x00);
    extgpio_set_direction(GPIOB, 0x00);

    extgpio_set(GPIOA, 0x00);
    extgpio_set(GPIOB, 0x00);

    ESP_LOGI(LOG_TAG, "Starting update task...");

    xTaskCreate(extgpio_update_task, "External GPIO Update Task", 2048, NULL, 5, update_task_handle);

    ESP_LOGI(LOG_TAG, "External GPIO Module over I2C (mcp23017) is initialised!");
}

void extgpio_set_direction(mcp23017_gpio_port_t gpio, uint8_t value) {
    LOCK(update_mutex, gpio_direction_list[gpio] = value);
}

void extgpio_set_pin_direction_in(mcp23017_gpio_port_t gpio, uint8_t pin) {

    LOCK(update_mutex, int8_t curr_dir = gpio_direction_list[gpio]);

    extgpio_set_direction(gpio, curr_dir | pin);

}

void extgpio_set_pin_direction_out(mcp23017_gpio_port_t gpio, uint8_t pin) {

    LOCK(update_mutex, int8_t curr_dir = gpio_direction_list[gpio]);
    
    extgpio_set_direction(gpio, curr_dir & ~pin);

}

void extgpio_set(mcp23017_gpio_port_t gpio, uint8_t value) {
    LOCK(update_mutex, gpio_out_value_list[gpio] = value);
}

void extgpio_set_pin_on(mcp23017_gpio_port_t gpio, uint8_t pin) {

    LOCK(update_mutex, int8_t curr_value = gpio_direction_list[gpio]);
    
    extgpio_set(gpio, curr_value | pin);

}

void extgpio_set_pin_off(mcp23017_gpio_port_t gpio, uint8_t pin) {

    LOCK(update_mutex, int8_t curr_value = gpio_direction_list[gpio]);

    extgpio_set(gpio, curr_value & ~pin);

}

uint8_t extgpio_get(mcp23017_gpio_port_t gpio) {

    LOCK(update_mutex, uint8_t result = gpio_in_value_list[gpio]);

    return result;
}

bool extgpio_get_pin(mcp23017_gpio_port_t gpio, uint8_t pin) {
    return extgpio_get(gpio) & pin ? true : false;
}

static void extgpio_update_task(void* params) {

    uint8_t curr_gpio_out_value_list[] = { 0x00, 0x00 };
    uint8_t curr_gpio_dir_list[] = { 0x00, 0x00 };

    uint8_t direction_value;
    uint8_t in_value;
    uint8_t out_value;

    while(true) {

        for (int gpio = 0; gpio < 2 /* GPIO row count */; gpio++) {
            
            // Get current values for current GPIO
            LOCK(update_mutex, {
                direction_value = gpio_direction_list[gpio];
                out_value = gpio_out_value_list[gpio];
            });

            // Update direction
            if(direction_value != curr_gpio_dir_list[gpio]) {
                LOCK(write_mutex, mcp23017_set_io_dir(mcp_dev, direction_value, gpio));
                curr_gpio_dir_list[gpio] = direction_value;
            }

            // Update in-direction values
            LOCK(write_mutex, in_value = mcp23017_read_io(mcp_dev, gpio) & direction_value);
            // in_value = mcp23017_read_io(mcp_dev, gpio) & direction_value;
            
            // Update out-direction values
            if(out_value != curr_gpio_out_value_list[gpio]) {
                LOCK(write_mutex, mcp23017_write_io(mcp_dev, out_value, gpio));
                //mcp23017_write_io(mcp_dev, out_value, gpio);
                curr_gpio_out_value_list[gpio] = out_value;
            }

            // Update current values for current GPIO
            LOCK(update_mutex, gpio_in_value_list[gpio] = in_value);
        }

        vTaskDelay(10 / portTICK_RATE_MS);
    }

    vTaskDelete(NULL);

}