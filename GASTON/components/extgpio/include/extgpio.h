
#ifndef EXTGPIO_H
#define EXTGPIO_H

#include <stdbool.h>

#include "mcp23017.h"

#define GPIOA MCP23017_GPIOA
#define GPIOB MCP23017_GPIOB

#define extgpio_pin(pin) (0x01 << (pin))
#define extgpio_portname(gpio) (gpio == GPIOA ? "GPIOA" : "GPIOB")

typedef mcp23017_gpio_port_t extgpio_port;

void extgpio_init();

void extgpio_set_direction(extgpio_port gpio, uint8_t value);
void extgpio_set_pin_direction_in(extgpio_port gpio, uint8_t pin);
void extgpio_set_pin_direction_out(extgpio_port gpio, uint8_t pin);

void extgpio_set(extgpio_port gpio, uint8_t value);
void extgpio_set_pin_on(extgpio_port gpio, uint8_t pin);
void extgpio_set_pin_off(extgpio_port gpio, uint8_t pin);

uint8_t extgpio_get(extgpio_port gpio);
bool extgpio_get_pin(extgpio_port gpio, uint8_t pin);

#endif