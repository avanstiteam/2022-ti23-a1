
#include <stdio.h>

#include "esp_log.h"

#include "sdkconfig.h"

#include "extgpio.h"
#include "custom_button.h"

#include "button_menu_controller.h"

#ifdef CONFIG_MENU_BUTTON_GPIO_A 
    #define BUTTON_GPIO GPIOA 
#else 
    #define BUTTON_GPIO GPIOB
#endif
    
static const char* LOG_TAG = "BUTTON_MENU_CONTROLLER";

static void (*onMoveCallback)(int16_t);
static void (*onClickCallback)(void);

static button_handle_t leftButton;
static button_handle_t clickButton;
static button_handle_t rightButton;

static void left_button_click(button_handle_t btn) {
    (*onMoveCallback)(-1);
}

static void click_button_click(button_handle_t btn) {
    (*onClickCallback)();
}

static void right_button_click(button_handle_t btn) {
    (*onMoveCallback)(1);
}

void button_menu_controller_init() {
    leftButton = btn_create(BUTTON_GPIO, extgpio_pin(CONFIG_MENU_BUTTON_LEFT_PIN));
    clickButton = btn_create(BUTTON_GPIO, extgpio_pin(CONFIG_MENU_BUTTON_CLICK_PIN));
    rightButton = btn_create(BUTTON_GPIO, extgpio_pin(CONFIG_MENU_BUTTON_RIGHT_PIN));

    btn_register_click_handler(leftButton, left_button_click);
    btn_register_click_handler(clickButton, click_button_click);
    btn_register_click_handler(rightButton, right_button_click);

    ESP_LOGI(LOG_TAG, "Initialised menu controller using buttons!");
}

void button_menu_controller_set_on_move(void (*onMove)(int16_t)) {
    onMoveCallback = onMove;
}

void button_menu_controller_set_on_click(void (*onClick)(void)) {
    onClickCallback = onClick;
}