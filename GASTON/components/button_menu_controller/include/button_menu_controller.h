
#ifndef BUTTON_MENU_CONTROLLER_H
#define BUTTON_MENU_CONTROLLER_H

void button_menu_controller_init();

void button_menu_controller_set_on_move(void (*onMove)(int16_t));
void button_menu_controller_set_on_click(void (*onClick)(void));

#endif