#include <stdio.h>
#include <time.h>
#include <stdbool.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"

#include "lcd.h"
#include "ntp.h"
#include "menu.h"
#include "sd_sound_player.h"
#include "audio_handeler.h"
#include "translation_system.h"
#include "timer_clock.h"
#include "alarm_clock.h"

#include "clock.h"

static const char* LOG_TAG = "clock";

static void clock_on_ntp_sync(struct timeval* tv);

static void clock_update_task(void* params);

static void clock_menu_lcd_show();
static void clock_time_lcd_show();

static const char* clock_date_month_str(int);

static void clock_play_wake_up_message_task(void*);
static void clock_play_current_time_task(void*);

static time_t now;
static struct tm timeInfo;

static bool clock_timeIsShown = true;
static bool clock_menuIsShown = false;

static TaskHandle_t clockUpdateTaskHandle;
static TaskHandle_t clockSpeakTimeTaskHandle;

static bool wakeupMessageIsPlayed = false;

#define CLOCK_TIME_STR "%02d:%02d:%02d"
#define CLOCK_TIME_TOSTR(x) x.tm_hour, \
    x.tm_min, \
    x.tm_sec

#define CLOCK_DATE_STR "%02d-%02d-%04d"
#define CLOCK_DATE_TOSTR(x) x.tm_mday, \
    x.tm_mon + 1, \
    x.tm_year + 1900

#define CLOCK_DATE_M_STR "%d %s, %04d"
#define CLOCK_DATE_M_TOSTR(x) x.tm_mday, \
    clock_date_month_str(x.tm_mon + 1), \
    x.tm_year + 1900

static int selected_language_clock;

void set_selected_language_clock(int selected_language) {
    selected_language_clock = selected_language;
}

void clock_init(int selected_language) {
    set_selected_language_clock(selected_language);
    ntp_init(clock_on_ntp_sync);

    time(&now);
    localtime_r(&now, &timeInfo);

    ESP_LOGI(LOG_TAG, "Starting clock update task");
    xTaskCreate(clock_update_task, "Clock Update Task", 2048 * 4, NULL, 5, &clockUpdateTaskHandle);

    ESP_LOGI(LOG_TAG, "Clock is initialized!");
}

void clock_menu_show() {
    time(&now);
    localtime_r(&now, &timeInfo);

    clock_timeIsShown = false;

    clock_menu_lcd_show();

    clock_menuIsShown = true;
}

void clock_menu_exit() {
    clock_menuIsShown = false;
    clock_timeIsShown = false;
}

void clock_start_speak_time() {
    xTaskCreatePinnedToCore(clock_play_current_time_task, "Play Time Task", 1024 * 4, NULL, 6, &clockSpeakTimeTaskHandle, 1);
}

void clock_stop_speak_time() {

    if(clockSpeakTimeTaskHandle != NULL) {
        vTaskDelete(clockSpeakTimeTaskHandle);
    }
    
}

static void clock_on_ntp_sync(struct timeval* tv) {

    time(&now);
    localtime_r(&now, &timeInfo);

    if(!wakeupMessageIsPlayed) {
        wakeupMessageIsPlayed = true;
        xTaskCreatePinnedToCore(clock_play_wake_up_message_task, "Wake Up Message", 5000, NULL, 6, NULL, 1);
    }

}

static void clock_update_task(void* params) {

    TickType_t last_wake_time = 0;

    while(1) {
        time(&now);
        localtime_r(&now, &timeInfo);

        if (get_alarm_state())
        {
            check_alarm_time(timeInfo.tm_hour, timeInfo.tm_min);
        }

        if (get_timer_state())
        {
            check_timer_time();
        }
    
        
        if(clock_menuIsShown) {
            clock_menu_lcd_show();
        }

        if(clock_timeIsShown) {
            clock_time_lcd_show();
        }

        vTaskDelayUntil(&last_wake_time, 1000 / portTICK_RATE_MS);
    }

    vTaskDelete(NULL);

}

static void clock_menu_lcd_show() {

    char strBuffer[100];

    lcd_takeMutex();
    lcd_setCursorToLine(0);
    lcd_write_string("De huidige tijd is:");

    sprintf(strBuffer, CLOCK_TIME_STR, CLOCK_TIME_TOSTR(timeInfo));

    lcd_setCursorToLine(1);
    lcd_write_string(strBuffer);

    lcd_setCursorToLine(2);
    lcd_write_string("De datum is:");

    sprintf(strBuffer, CLOCK_DATE_M_STR, CLOCK_DATE_M_TOSTR(timeInfo));

    lcd_setCursorToLine(3);
    lcd_write_string(strBuffer);
    lcd_giveMutex();

}

static void clock_time_lcd_show() {
    char strBuffer[6];

    sprintf(strBuffer, "%02d%s%02d",
        timeInfo.tm_hour,
        timeInfo.tm_sec % 2 ? ":" : " ", // Blink the ':' every second
        timeInfo.tm_min
    );

    lcd_takeMutex();
    lcd_setCursor(15, 0);
    lcd_write_string(strBuffer);
    lcd_giveMutex();
}

static void clock_play_wake_up_message_task(void* params) {
    char *path;

    
    if (timeInfo.tm_hour >= 0 && timeInfo.tm_hour < 12) {
        path = "mo";
    }
    else if (timeInfo.tm_hour >= 12 && timeInfo.tm_hour < 18) {
        path = "af";
    }
    else {
        path = "ev";
    }

    // Lead playAudio to correct path for selected language.
    char *to_play = get_language_path(selected_language_clock, path);
    audio_handler_playAudio(AUDIO_PIPE_USESD, to_play);

    // This function will close the current task after running
    clock_play_current_time_task(NULL);
}

static void clock_play_current_time_task(void* params) {

    // TODO: Lead playAudio to correct path for selected language.
    char hoursString[3];
    char minutesString[3];

    sprintf(hoursString, "%d", timeInfo.tm_hour);
    sprintf(minutesString, "%d", timeInfo.tm_min);

    audio_handler_playAudio(AUDIO_PIPE_USESD, get_language_path(selected_language_clock, hoursString));

    char *path_hour = "Uur";
    audio_handler_playAudio(AUDIO_PIPE_USESD, get_language_path(selected_language_clock, path_hour));

    if (timeInfo.tm_min != 0) {
        audio_handler_playAudio(AUDIO_PIPE_USESD, get_language_path(selected_language_clock, minutesString));
    }

    //audio_handler_startRecognition();
    audio_handler_playAudio(AUDIO_PIPE_USEVOICE, NULL);
    vTaskDelete(NULL);

}

static const char* clock_date_month_str(int month) {

    switch (month) {
        case 1:
            return "Januari";
            break;
        case 2:
            return "Februari";
            break;
        case 3:
            return "Maart";
            break;
        case 4:
            return "April";
            break;
        case 5:
            return "Mei";
            break;
        case 6:
            return "Juni";
            break;
        case 7:
            return "Juli";
            break;
        case 8:
            return "Augustus";
            break;
        case 9:
            return "September";
            break;
        case 10:
            return "Oktober";
            break;
        case 11:
            return "November";
            break;
        case 12:
            return "December";
            break;
        default:
            return "Error";
            break;
    }

}
