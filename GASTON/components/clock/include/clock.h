
#ifndef CLOCK_H
#define CLOCK_H

void clock_init(int selected_language);

void clock_menu_show();
void clock_menu_exit();

void clock_start_speak_time();
void clock_stop_speak_time();

void set_selected_language_clock(int selected_language);

#endif
