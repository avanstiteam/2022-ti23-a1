#include <stdio.h>
#include "TouchButtons.h"
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "i2s_stream.h"
#include "esp_peripherals.h"
#include "periph_touch.h"
#include "periph_adc_button.h"
#include "periph_button.h"
#include "board.h"
#include "filter_resample.h"
#include "audio_mem.h"

static const char *TAG = "TOUCH SENSORS";

TaskHandle_t touchTask;
audio_event_iface_handle_t evt;
int attempt = 0;

void initTouch();
void start_check(void * parameters);

void startTouch() {
    xTaskCreatePinnedToCore(start_check, "Touch Button", 20000, NULL, 6, &touchTask, 1);
}

void initTouch()
{
    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set(TAG, ESP_LOG_DEBUG);
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);

    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    esp_periph_set_handle_t set = esp_periph_set_init(&periph_cfg);

    periph_touch_cfg_t config = {
        .touch_mask = BIT(get_input_set_id()) | BIT(get_input_play_id()) | BIT(get_input_volup_id()) | BIT(get_input_voldown_id()),
        .tap_threshold_percent = 90,
    };

    esp_periph_handle_t touch_periph = periph_touch_init(&config);
    periph_button_cfg_t button = {
        .gpio_mask = (1ULL << get_input_rec_id()) | (1ULL << get_input_mode_id()),
    };

    esp_periph_handle_t button_handle = periph_button_init(&button);

    esp_periph_start(set, touch_periph);
    esp_periph_start(set, button_handle);

    evt = audio_event_iface_init(&evt_cfg);
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);

    attempt = 1;
}

void start_check(void * parameters){
    while (1) {       
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        printf("Button test \n");       

        if (ret != ESP_OK)
        {
            printf("Niet oke \n");
            continue;
        }

        if (attempt >= 1 || attempt < 0) attempt = 0;
        else attempt++;

        printf("amount: %d \n", attempt);
    }
}
