
#ifndef NTP_H
#define NTP_H

#include <time.h>

typedef void (*NtpSyncHandler_t)(struct timeval*);

void ntp_init(NtpSyncHandler_t syncHandler);

void ntp_force_sync();

#endif