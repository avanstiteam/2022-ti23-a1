/*

    Remarks: sntp updates the internal clock once an hour by default.
        This behaviour can be changed in menuconfig under:
            (Top) -> Component Config -> LWIP -> SNTP

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "esp_system.h"
#include "esp_sntp.h"
#include "esp_log.h"

#include "common.h"

#include "ntp.h"

#define UNIX_VALID_TIME 1577836800 /* GMT 01/01/2022 00:00:00 */

static const char* LOG_TAG = "ntp";

static NtpSyncHandler_t ntpSyncCallback;

static void ntp_retry_sync_task(void* context);

static void ntp_time_sync_callback(struct timeval* tv) {
    time_t syncResult = tv->tv_sec;

    struct tm timeInfo;
    localtime_r(&syncResult, &timeInfo);

    if(syncResult > UNIX_VALID_TIME) {
        ESP_LOGI(LOG_TAG, "Got sync update from ntp server. Adjusting time to: %02d-%02d-%04d %02d:%02d:%02d",
            timeInfo.tm_mday,
            timeInfo.tm_mon + 1,
            timeInfo.tm_year + 1900,
            timeInfo.tm_hour,
            timeInfo.tm_min,
            timeInfo.tm_sec
        );

        if(ntpSyncCallback != NULL) 
            (*ntpSyncCallback)(tv);
    }
    else {
        ESP_LOGI(LOG_TAG, "Got invalid time (%02d-%02d-%04d %02d:%02d:%02d), retrying in 5 seconds...",
            timeInfo.tm_mday,
            timeInfo.tm_mon + 1,
            timeInfo.tm_year + 1900,
            timeInfo.tm_hour,
            timeInfo.tm_min,
            timeInfo.tm_sec
        );

        task_run_after(ntp_retry_sync_task, 5000 / portTICK_RATE_MS, NULL, 2048);
    }
}

void ntp_init(NtpSyncHandler_t syncHandler) {
    sntp_servermode_dhcp(1);

    // Source for TZ environment variabele value:
    // https://unix.stackexchange.com/questions/19935/timezone-time-setting-in-linux-using-shellscript/19957#19957
    setenv("TZ", "CET-1CEST,M3.5.0/2,M10.5.0/3", 1);
    tzset();

    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_set_sync_mode(SNTP_SYNC_MODE_SMOOTH);
    sntp_setservername(0, "pool.ntp.org");

    ESP_LOGI(LOG_TAG, "Registering ntpSyncCallback");
    ntpSyncCallback = syncHandler;
    sntp_set_time_sync_notification_cb(ntp_time_sync_callback);

    sntp_init();

    ESP_LOGI(LOG_TAG, "NTP-Service started!");
}

void ntp_force_sync() {
    ESP_LOGI(LOG_TAG, "Forcing resync of current time using ntp...");

    sntp_restart();
}

static void ntp_retry_sync_task(void* context) {
    time_t now;
    time(&now);

    if(now <= UNIX_VALID_TIME) {
        ntp_force_sync();
    }
}