
#include <stdio.h>
#include <stdbool.h>

#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "common.h"

#include "wifi.h"

static const char* LOG_TAG = "WiFi";

#define esp_assert(cond, msg) if((result = (cond)) != ESP_OK) { \
    ESP_LOGE(LOG_TAG, msg " => %s", esp_err_to_name(result)); \
    return result; \
}

#define esp_assert_log(cond, msg) if((result = (cond)) != ESP_OK) { \
    ESP_LOGE(LOG_TAG, msg " => %s", esp_err_to_name(result)); \
}

static esp_event_handler_instance_t instance_any_id;
static esp_event_handler_instance_t instance_got_ip;

static EventHandler_t connectHandler;
static EventHandler_t disconnectHandler;
static GotIpEventHandler_t gotIpHandler;

static bool wifiConnected;

static void wifi_event_handler(
    void* args,
    esp_event_base_t eventBase,
    int32_t eventId,
    void* eventData
);

static void wifi_retry_connect_task(void* context);

esp_err_t wifi_init() {
    esp_err_t result = ESP_OK;

    connectHandler = NULL;
    disconnectHandler = NULL;
    gotIpHandler = NULL;

    wifiConnected = false;

    esp_assert(esp_netif_init(), "wifi_init(): esp_netifinit() failed");

    result = esp_event_loop_create_default();
    if(!(result == ESP_OK || result == ESP_ERR_INVALID_STATE)) {
        ESP_LOGW(LOG_TAG, "wifi_init(): esp_event_loop_create_default() failed => %s", esp_err_to_name(result));
    }

    esp_netif_create_default_wifi_sta();

    wifi_init_config_t wifiInitConfig = WIFI_INIT_CONFIG_DEFAULT();
    esp_assert(
        esp_wifi_init(&wifiInitConfig),
        "wifi_init(): esp_wifi_init() failed"
    );

    esp_assert(
        esp_wifi_set_default_wifi_sta_handlers(),
        "wifi_init(): esp_wifi_set_default_wifi_sta_handlers() failed"
    );

    esp_assert_log(
        esp_event_handler_instance_register(WIFI_EVENT,
                                            ESP_EVENT_ANY_ID,
                                            &wifi_event_handler,
                                            NULL,
                                            &instance_any_id),
        "wifi_init(): esp_event_handler_instance_register(WIFI_EVENT) failed, moving on without event handler"
    );

    esp_assert_log(
        esp_event_handler_instance_register(IP_EVENT,
                                            IP_EVENT_STA_GOT_IP,
                                            &wifi_event_handler,
                                            NULL,
                                            &instance_got_ip),
        "wifi_init(): esp_event_handler_instance_register(IP_EVENT) failed, moving on without event handler"
    );

    return result;
}

esp_err_t wifi_connect() {
    esp_err_t result = ESP_OK;
    
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = CONFIG_WIFI_SSID,
            .password = CONFIG_WIFI_PASSWORD
        },
    };

    esp_assert(esp_wifi_set_mode(WIFI_MODE_STA), "wifi_start(): esp_wifi_set_mode(WIFI_MODE_STA) failed");
    esp_assert(esp_wifi_set_config(WIFI_IF_STA, &wifi_config), "wifi_start(): esp_wifi_set_config() failed");
    
    esp_assert(esp_wifi_start(), "wifi_start(): esp_wifi_start() failed");
    esp_assert(esp_wifi_connect(), "wifi_start(): esp_wifi_connect() failed");

    return result;
}

void wifi_register_connect_handler(EventHandler_t handler) {
    connectHandler = handler;
}

void wifi_register_disconnect_handler(EventHandler_t handler) {
    disconnectHandler = handler;
}

void wifi_register_got_ip_handler(GotIpEventHandler_t handler) {
    gotIpHandler = handler;
}

bool wifi_is_connected() {
    return wifiConnected;
}

static void wifi_event_handler(
    void* args,
    esp_event_base_t eventBase,
    int32_t eventId,
    void* eventData
) {

    if(eventBase == WIFI_EVENT) {

        if(eventId == WIFI_EVENT_STA_CONNECTED) {
            ESP_LOGI(LOG_TAG, "Connected successfully to AP " CONFIG_WIFI_SSID);

            if(connectHandler != NULL)
                (*connectHandler)();
        }
        else if(eventId == WIFI_EVENT_STA_DISCONNECTED) {
            ESP_LOGW(LOG_TAG, "Connecting to AP " CONFIG_WIFI_SSID " failed or connection was lost, retrying in 5 seconds...");
            
            task_run_after(wifi_retry_connect_task, 5000 / portTICK_RATE_MS, NULL, 4096);

            if(disconnectHandler != NULL)
                (*disconnectHandler)();
        }

    }
    if(eventBase == IP_EVENT) {

        if(eventId == IP_EVENT_STA_GOT_IP) {
            ip_event_got_ip_t* event = (ip_event_got_ip_t*) eventData;
            ESP_LOGI(LOG_TAG, "Got IP-Address (" IPSTR ")", IP2STR(&event->ip_info.ip));

            wifiConnected = true;

            if(gotIpHandler != NULL)
                (*gotIpHandler)(event->ip_info.ip);
        }

    }

}

static void wifi_retry_connect_task(void* context) {
    esp_wifi_connect();
}