
#ifndef WIFI_H
#define WIFI_H

#include "esp_wifi.h"

#include "common.h"

typedef void (*GotIpEventHandler_t)(esp_ip4_addr_t ipAddress);

esp_err_t wifi_init();

esp_err_t wifi_connect();

void wifi_register_connect_handler(EventHandler_t handler);
void wifi_register_disconnect_handler(EventHandler_t handler);
void wifi_register_got_ip_handler(GotIpEventHandler_t handler);

bool wifi_is_connected();

#endif