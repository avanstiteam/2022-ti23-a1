#ifndef LCD_H

#define LCD_H
#include "i2c_conf.h"
#include "driver/i2c.h"
#include "i2c-lcd1602.h"

void lcd_setCursor(int column, int row);
void lcd_setCursorToLine(int line);
/**
 * @brief This method is used to write a char * / string to the lcd.
 * It will use the current cursor position.
 * 
 * @param str is a char * to be written onto the lcd.
 */
void lcd_write_string(const char *str);
/**
 * @brief This method is used tp initialize the lcd screen.
 * 
 * @param _conf is a i2c config. IS REDUNDANT.
 */
void lcd_init();
/**
 * @brief This method is used to clear the lcd screen.
 */
void lcd_clear();
/**
 * @brief This method is used to hide the cursor of the lcd screen.
 */
void lcd_removeCursor();
/**
 * @brief This method is used to set the cursor to position 0,0.
 * 
 */
void lcd_home();
/**
 * @brief This method is used to take the lcd mutex.
 * Use this method if you want to start using the lcd.
 */
void lcd_takeMutex();
/**
 * @brief This method is used to give the lcd mutex.
 * Use this method if you are done using the lcd and you called the takeMutex method.
 * Is currently not safe. May be called by something else and give away someone elses mutex.
 */
void lcd_giveMutex();

void lcd_defince_custom_character(i2c_lcd1602_custom_index_t char_index, uint8_t pixels[8]);

void lcd_write_char(char custom);

void lcd_clear_section(int start_index, int end_index, int line);

#endif