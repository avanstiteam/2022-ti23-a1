#include <stdio.h>
#include "smbus.h"
#include "i2c-lcd1602.h"
#include "i2c_conf.h"
#include "freertos/semphr.h"

#include "i2c_bus.h"

#include "lcd.h"

#ifndef LYRAT_SCK
#define LYRAT_SCK 23
#endif
#ifndef LYRAT_SDA
#define LYRAT_SDA 18
#endif

#define LCD_NUM_ROWS               4
#define LCD_NUM_COLUMNS            16
#define LCD_NUM_VISIBLE_COLUMNS    20

#define I2C_MASTER_NUM           I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled
#define I2C_MASTER_FREQ_HZ       100000
#define I2C_MASTER_SDA_IO        LYRAT_SDA
#define I2C_MASTER_SCL_IO        LYRAT_SCK
#ifndef LYRAT_SCK_SPEED
#define LYRAT_SCK_SPEED 10000
#endif

#ifndef LCD_I2C_ADDR
#define LCD_I2C_ADDR 0x27
#endif

int i2c_master_port = I2C_MASTER_NUM;

i2c_lcd1602_info_t * lcd_info;

SemaphoreHandle_t mutex;

static uint8_t _wait_for_user(void)
{
    uint8_t c = 0;

#ifdef USE_STDIN
    while (!c)
    {
       STATUS s = uart_rx_one_char(&c);
       if (s == OK) {
          printf("%c", c);
       }
       vTaskDelay(1);
    }
#else
    vTaskDelay(1000 / portTICK_RATE_MS);
#endif
    return c;
}

void lcd_write_string(const char *str) {
	i2c_lcd1602_write_string(lcd_info, str);
}

void lcd_home() {
    i2c_lcd1602_home(lcd_info);
}

// This method is used to set the cursor of an lcd to and column and row.
// Column is horizontal and row is vertical.
void lcd_setCursor(int column, int row) {
    i2c_lcd1602_move_cursor(lcd_info, column, row);
}

void lcd_setCursorToLine(int line) {
    i2c_lcd1602_move_cursor(lcd_info, 0, line);
}

void lcd_clear() {
    i2c_lcd1602_clear(lcd_info);
}

void lcd_removeCursor() {
    i2c_lcd1602_set_cursor(lcd_info, false);
}

void lcd_takeMutex() {
    xSemaphoreTake(mutex, portMAX_DELAY);
}

void lcd_giveMutex() {
    xSemaphoreGive(mutex);
}

void lcd_init() {
    // conf.mode = I2C_MODE_MASTER;
    // conf.sda_io_num = I2C_MASTER_SDA_IO;
    // conf.sda_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    // conf.scl_io_num = I2C_MASTER_SCL_IO;
    // conf.scl_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    // conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    // i2c_param_config(i2c_master_port, &conf);
    // i2c_driver_install(i2c_master_port, conf.mode,
    //                    I2C_MASTER_RX_BUF_LEN,
    //                    I2C_MASTER_TX_BUF_LEN, 0);
    
    i2c_port_t i2c_num = I2C_NUM_0;
    uint8_t address = LCD_I2C_ADDR;
    smbus_info_t * smbus_info = smbus_malloc();
    ESP_ERROR_CHECK(smbus_init(smbus_info, i2c_num, address));

    ESP_ERROR_CHECK(smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS));

    // Set up the LCD1602 device with backlight off
    lcd_info = i2c_lcd1602_malloc();
    ESP_ERROR_CHECK(i2c_lcd1602_init(lcd_info, smbus_info, true,
                                     LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VISIBLE_COLUMNS));

    ESP_ERROR_CHECK(i2c_lcd1602_reset(lcd_info));

    // turn off backlight
    // ESP_LOGI(TAG, "backlight off");
    _wait_for_user();
    i2c_lcd1602_set_backlight(lcd_info, true);
    i2c_lcd1602_set_blink(lcd_info, false);
    i2c_lcd1602_set_cursor(lcd_info, false);
    i2c_lcd1602_set_auto_scroll(lcd_info, false);
    i2c_lcd1602_set_left_to_right(lcd_info);
    mutex = i2c_mutex;
}

void lcd_defince_custom_character(i2c_lcd1602_custom_index_t char_index, uint8_t pixels[]) {
    i2c_lcd1602_define_char(lcd_info, char_index, pixels);
}

void lcd_write_char(char custom) {
    i2c_lcd1602_write_char(lcd_info, custom);
}

void lcd_clear_section(int start_index, int end_index, int line) {
    for (size_t i = start_index; i <= end_index; i++)
    {
        lcd_setCursor(i, line);
        lcd_write_string(" ");
    }
    
}