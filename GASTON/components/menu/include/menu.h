#ifndef MENU_H
#define MENU_H
#include "i2c_conf.h"
#include "driver/i2c.h"


typedef struct {
    char* itemText;
    int openMenuID;
    void (*menu_callback)(void *);
} MENU_ITEM;


typedef struct {
    int id;
    int menu_size;
    MENU_ITEM items[5]; //TODO MAKE MODULAR
} MENU;

/**
 * @brief This method is used to draw the menu.
 * Currently the input should be an int casted to (void*).
 */
void menu_draw(void *);
/**
 * @brief This method is used to start the internet radio.
 * 
 */
void menu_startRadio(void *);
/**
 * @brief This method is used to initialize the menu.
 * 
 * @param _conf REDUNDANT. IS TO BE REMOVED!!
 */
void menu_init(int selected_language);
/**
 * @brief This method is used to change the current cursor position in the menu.
 * 
 * @param i Is the amount moved by for example the rotary encoder.
 */
void menu_changePos(int i);
/**
 * @brief This method is used to call the function pointer of the menu item.
 * 
 */
void menu_checkMethod();
void playTime();
void radio_morseCode();
int get_settint_time();
void update_alarm_time(int16_t val);
void change_hour_minute_state();
int get_setting_timer();
void update_timer_time(int16_t val);
void change_minute_second_state();
void radio_onClap(int amount);
void process_selected_language_menu(int selected_language);

#endif
