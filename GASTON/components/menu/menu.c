#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lcd.h"
#include "i2c_conf.h"
#include "radio.h"
#include "clock.h"
#include "sd_sound_player.h"
#include "audio_recognition.h"
#include "esp_log.h"
#include "audio_handeler.h"
#include "alarm_clock.h"
#include "timer_clock.h"
#include "translation_system.h"
#include "menu.h"
#include "morsemenu.h"

int setting_time = 0;
int setting_hours = 1;
int setting_minutes = 0;

int setting_timer = 0;
int setting_timer_minutes = 1;
int setting_timer_seconds = 0;

int alarm_minutes = 0;
int alarm_hours = 0;

int timer_minutes = 0;
int timer_seconds = 0;

int get_settint_time() {
    return setting_time;
}

int get_setting_timer() {
    return setting_timer;
}

void checkRadio();
void confirmStation();
void checkPos(int i);
void exitRadio();
void set_alarm();
void set_timer();

MENU menus[] = {
    {
        0,
        4,
        {
            {"Muziek", 1, &menu_draw},
            {"Klok", 2, &menu_draw},
            {"Morsecode", -1, &morse_menu_show},
            {"Taal", 7, &menu_draw}
        }
    },
    {
        1,
        3,
        {
            {"Terug", 0, &menu_draw},
            {"Radio", 3, &checkRadio},
            {"Bluetooth", 2, &menu_draw}
        }
    },
    {
        2,
        4,
        {
            {"Terug", 0, &menu_draw},
            {"Luister tijd", 0, &clock_start_speak_time},
            {"Timer", 6, &menu_draw},
            {"Wekker", 5, &menu_draw}
        }
    },
    {
        3,
        4,
        {
            {"Terug", 1, &menu_draw},
            {"Radio 538", 0, &confirmStation},
            {"Slam", 0, &confirmStation},
            {"SkyRadio", 0, &confirmStation}
        }
    },
    {
        4,
        3,
        {
            {"Terug", 1, &exitRadio},
            {"Selecteer zender", 3, &menu_draw},
            {"Huidig:", -1, NULL}
        }
    },
    {
        5,
        3,
        {
            {"Terug", 2, &menu_draw},
            {"00:00", 5, NULL},
            {"Bevestig", 1, &set_alarm}
        }
    },
    {
        6,
        3,
        {
            {"Terug", 2, &menu_draw},
            {"00:00", 5, NULL},
            {"Bevestig", 1, &set_timer}
        }
    },
    {
        7,
        4,
        {
            {"Terug", 0, &menu_draw},
            {"English", 0, &set_language_to_english},
            {"Nederlands", 0, &set_language_to_dutch},
            {"Volgende", 8, &menu_draw}
        }
    },
    {
        8,
        3,
        {
            {"Terug", 7, &menu_draw},
            {"Deutsch", 0, &set_language_to_german},
            {"Francais", 0, &set_language_to_french}
        }
    },
    {
        9,
        4,
        {
            { "Terug", 0, &morse_menu_exit },
            { "Speed: ", -1, NULL },
            { "Frequency: ", -1, NULL },
            { "Bevestig", 0, &morse_menu_start_detect }
        }
    },
    {
        10,
        1,
        { 
            { "Terug", 0, &morse_menu_stop_detect }
        }
    }
};

static MENU currentMenu;
static int position = 0;
int currentMenuSize = 0;

void menu_draw(void * menuID) {
    int pos = (int)menuID;

    currentMenu = menus[pos];
    MENU_ITEM* items = currentMenu.items;
    currentMenuSize = currentMenu.menu_size;
    
    lcd_takeMutex();
    lcd_clear();
    for (int i = 0; i < currentMenuSize; i++) {
        if (items[i].itemText != NULL) {
            lcd_setCursor(1, i);
            lcd_write_string(items[i].itemText);
        }
    }

    lcd_setCursor(0, position = 0);
    lcd_write_char(0x7E);
    lcd_giveMutex();
}

void menu_changePos(int i) {
    lcd_takeMutex();
    lcd_setCursor(0, position);
    lcd_write_string(" ");
    checkPos(i);

    lcd_setCursor(0, position);
    lcd_write_char(0x7E);
    lcd_giveMutex();
}

void process_selected_language_menu(int selected_language) {
    process_selected_language_for_menu(menus, selected_language);
}

void menu_init(int selected_language) {
    process_selected_language_menu(selected_language);
    currentMenu = menus[0];
    lcd_init();
}

void menu_checkMethod() {
    MENU_ITEM* items = currentMenu.items;

    MENU_ITEM currentItem = items[position];
    if(currentItem.menu_callback != NULL) {
        currentItem.menu_callback((void*)currentItem.openMenuID);

        if (strcmp(currentItem.itemText, "Wekker") == 0 || strcmp(currentItem.itemText, "Alarm") == 0 || strcmp(currentItem.itemText, "Wecker") == 0)
        {
            setting_time = 1;
        }

        if (strcmp(currentItem.itemText, "Timer") == 0 || strcmp(currentItem.itemText, "Le minuteur") == 0)
        {
            setting_timer = 1;
        }
        
    }
}

static int init = 0;
void checkRadio(void * parameters) {
    if (init == 0) {
        menu_draw((void*)3);
    } else {
        menu_draw((void*)4);
        //radio_start();
        // if(radio_isRunning)
        // audio_handler_stopAudio();
        audio_handler_playAudio(AUDIO_PIPE_USERADIO, AAC_STREAM_URI);
    }
}

void confirmStation(void * parameters) {
    printf("Changing station!\n");
    radio_changeStation(position - 1);
    printf("Position: %d", (position - 1));

    init = 1;

    menu_draw((void*)4);
    // if(radio_isRunning)
    // audio_handler_stopAudio();
    audio_handler_playAudio(AUDIO_PIPE_USERADIO, AAC_STREAM_URI);
}

void checkPos(int i) {
    position += i;
    // printf("Amount: %d", amount);
    printf("Current position: %d\n", position);
    if (position >= (currentMenuSize)) {
        position = 0;
    } else if (position < 0) {
        position = currentMenuSize - 1;
    }

    printf("Current position: %d\n", position);
}

void exitRadio() {
    printf("EXITING RADIO\n");
    audio_handler_stopAudio();
    audio_handler_playAudio(AUDIO_PIPE_USEVOICE, NULL);
    menu_draw((void *)1);
}

/**
 * @brief Needs to be called from task.
 * 
 */
void playTime() {
    clock_start_speak_time();
}

void radio_onClap(int amount) {
    ESP_LOGI("*", "Got sound callback with amount %d", amount);
    switch (amount)
    {
    case 1:
        menu_changePos(1);
        break;
    case 2:
        menu_checkMethod();
        break;

    case 3:
        menu_changePos(-1);
        break;    
    default:
        break;
    }
}

void radio_morseCode() {
    //audio_handler_startRecognition();
}

void set_alarm() {
    set_alarm_time(alarm_hours, alarm_minutes);
    set_alarm_state(1);

    menu_draw(0);
    drawTime();
}

void set_timer() {
    set_timer_time(timer_minutes, timer_seconds);
    set_timer_state(1);

    menu_draw(0);
}

void update_alarm_time(int16_t val) {
    char time_string[80];

    if (setting_hours) {
        alarm_hours += val;

        if (alarm_hours == 24)
        {
            alarm_hours = 0;
        } else if (alarm_hours < 0)
        {
            alarm_hours = 23;
        }
        
        
    } else if (setting_minutes)
    {
        alarm_minutes += val;

        if (alarm_minutes == 60) {
            alarm_minutes = 0;
        } else if (alarm_minutes < 0)
        {
            alarm_minutes = 59;
        }
        
    }

    lcd_takeMutex();

    sprintf(time_string, "%02d:%02d ", alarm_hours, alarm_minutes);
    lcd_setCursor(1, 1);
    lcd_write_string(time_string);

    lcd_giveMutex();
}

void update_timer_time(int16_t val) {
    char time_string[80];

    if (setting_timer_minutes) {
        timer_minutes += val;

        if (timer_minutes == 60)
        {
            timer_minutes = 0;
        } else if (timer_minutes < 0)
        {
            timer_minutes = 60;
        }
        
        
    } else if (setting_timer_seconds)
    {
        timer_seconds += val;

        if (timer_seconds == 60) {
            timer_seconds = 0;
        } else if (timer_seconds < 0)
        {
            timer_seconds = 59;
        }
        
    }

    lcd_takeMutex();

    sprintf(time_string, "%02d:%02d", timer_minutes, timer_seconds);
    lcd_setCursor(1, 1);
    lcd_write_string(time_string);

    lcd_giveMutex();
}

void change_hour_minute_state() {
    if (setting_hours)
    {
        setting_hours = 0;
        setting_minutes = 1;
    } else if (setting_minutes) {
        setting_minutes = 0;
        setting_time = 0;
        menu_changePos(2);
    }
    
}

void change_minute_second_state() {
    if (setting_timer_minutes)
    {
        setting_timer_minutes = 0;
        setting_timer_seconds = 1;
    } else if (setting_timer_seconds) {
        setting_timer_seconds = 0;
        setting_timer = 0;
        menu_changePos(2);
    }
    
}
