#ifndef AUDIO_RECOGNITION

#include "audio_pipeline.h"
#include "esp_peripherals.h"
#include "audio_event_iface.h"

/**
 * @brief This method is used to initialize the audio recognition.
 * 
 * @param _pipeline is a audio_pipeline_handle_t.
 */
void audio_recognition_init(audio_pipeline_handle_t  *_pipeline);
/**
 * @brief This method is used to start up the audio recognition.
 * 
 */
void audio_recognition_start(void);
/**
 * @brief This method is used to set the clap function callback.
 * 
 * @param _clapCallback is a function pointer with the parameters (int).
 */
void audio_recognition_setOnClap(void(*_clapCallback)(int amount));
/**
 * @brief This method is used to stop the audio recognition.
 * 
 */
void audio_recognition_stop(void);
/**
 * @brief This method is used to check if the audio recognition is currently running.
 * 
 * @return true 
 * @return false 
 */
bool audio_recognition_isRunning(void);

#endif
