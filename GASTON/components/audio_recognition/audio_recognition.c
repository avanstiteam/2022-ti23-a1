#include <stdio.h>
#include <math.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "filter_resample.h"
#include "esp_vad.h"
#include "audio_handeler.h"

#include "audio_recognition.h"
#include "amplitude-processing.h"
#include "audio_visualizer.h"

TaskHandle_t audio_recog_startHandle;
void audio_recognition_startTask(void * parameter); 

static const char *TAG = "AUDIO_RECOGNITION";

#define RMS_SAMPLE_RATE_HZ 8000
#define RMS_FRAME_LENGTH_MS 50

#define RMS_BUFFER_LENGTH (RMS_FRAME_LENGTH_MS * RMS_SAMPLE_RATE_HZ / 1000)

#define AUDIO_SAMPLE_RATE 48000         // Audio capture sample rate [Hz]

#define AUDIO_RECOGNITION_MAX_CLAPS 3
#define AUDIO_RECOGNITION_MAX_CLAP_DELAY 1500
#define AUDIO_RECOGNITION_CLAP_DELAY 300
#define AUDIO_RECOGNITION_DECIBEL_TRIGGER 70

bool clapIsRunning = false;

int16_t *raw_buffer;

audio_pipeline_handle_t pipeline;
audio_element_handle_t i2s_stream_reader;
audio_element_handle_t resample_filter;
audio_element_handle_t raw_reader;

static bool clapCounterIsRunning = false;

void audio_recognition_init(audio_pipeline_handle_t  *_pipeline) {
    pipeline = *_pipeline;
    ESP_LOGI(TAG, "Create audio elements for pipeline");
    i2s_stream_reader = audio_handler_get_i2s_stream_reader();
    resample_filter = audio_handler_get_resample_filter();
    raw_reader = audio_handler_get_raw_reader();  
} 

void audio_recognition_stop(void) {
    clapIsRunning = false;
    ESP_LOGI(TAG, "Deallocate raw sample buffer memory");
    free(raw_buffer);

    audio_pipeline_stop(pipeline);
    audio_pipeline_wait_for_stop(pipeline);
    audio_pipeline_terminate(pipeline);

    audio_pipeline_unregister(pipeline, i2s_stream_reader);
    audio_pipeline_unregister(pipeline, resample_filter);
    audio_pipeline_unregister(pipeline, raw_reader);

    ESP_LOGI(TAG, "[ 6.3 ] Removing listeners");
    audio_pipeline_remove_listener(pipeline);

    // Write zero decibels so leds turn of.
    audio_visualizer_write(0);
}
bool audio_recognition_isRunning(void) {
    return clapIsRunning;
}

void(*clapCallback)(int amount);
void audio_recognition_setOnClap(void(*_clapCallback)(int amount)) {
    clapCallback = _clapCallback;
}

static void audio_recognition_onClap(int amount) {
    clapCallback(amount);
}

static int claps = 0;
static TaskHandle_t clapCounter_handle;
/**
 * @brief This method is used for counting the claps made by a user.
 * 
 * @param parameter Is not used. Is needed for a task.
 */
static void audio_recognition_clapCounterTask(void * parameter) {
    static int oldClaps = 0;
    oldClaps = 0;
    printf("CLAPS RUNNING!\n");
    clapCounterIsRunning = true;
    while(claps < AUDIO_RECOGNITION_MAX_CLAPS) {
        vTaskDelay(AUDIO_RECOGNITION_MAX_CLAP_DELAY / portTICK_RATE_MS);
        printf("Amount of claps: %d\n", claps);
        if(oldClaps == claps) {
            printf("clap delay ran out!\n");
            break;
        }
        oldClaps = claps;
    }
    if(claps > AUDIO_RECOGNITION_MAX_CLAPS) claps = AUDIO_RECOGNITION_MAX_CLAPS;
    audio_recognition_onClap(claps);
    claps = 0;
    clapCounterIsRunning = false;
    vTaskDelete(NULL);
}

void audio_recognition_startTask(void * parameter) {
    esp_log_level_set(TAG, ESP_LOG_INFO);
    audio_visualizer_init();
    ESP_LOGI(TAG, "Create raw sample buffer");
    raw_buffer = (int16_t *) malloc((RMS_BUFFER_LENGTH * sizeof(int16_t)));
    if (raw_buffer == NULL) {
        ESP_LOGE(TAG, "Memory allocation for raw sample buffer failed");
        return;
    }

    ESP_LOGI(TAG, "Register audio elements to pipeline");
    audio_pipeline_register(pipeline, i2s_stream_reader, "i2s");
    audio_pipeline_register(pipeline, resample_filter, "rsp_filter");
    audio_pipeline_register(pipeline, raw_reader, "raw");

    ESP_LOGI(TAG, "Link audio elements together to make pipeline ready");
    const char *link_tag[3] = {"i2s", "rsp_filter", "raw"};
    audio_pipeline_link(pipeline, &link_tag[0], 3);

    ESP_LOGI(TAG, "Start pipeline");
    audio_pipeline_run(pipeline);
    claps = 0;
    clapIsRunning = true;

    while (clapIsRunning) {
        // Read the raw data and put it inside a buffer.
        raw_stream_read(raw_reader, (char *) raw_buffer, RMS_BUFFER_LENGTH * sizeof(int16_t));

        // Process the raw data and turn it into decibel.
        double decibel = amplitude_processing_process(raw_buffer, RMS_BUFFER_LENGTH);
        // Visualize the audio.
        audio_visualizer_write(decibel);
        if(decibel > AUDIO_RECOGNITION_DECIBEL_TRIGGER) {
            ESP_LOGI(TAG, "DB: %f", decibel);
            // If the claps aren't already being counted then start a task to count.
            if(!clapCounterIsRunning) {
                xTaskCreatePinnedToCore(audio_recognition_clapCounterTask, "Audio clapCounter task", 1024 * 4, NULL, 5, &clapCounter_handle, 0);
            }
            claps++;
        }
    }
    vTaskDelete(NULL);
}

void audio_recognition_start(void) {
    xTaskCreatePinnedToCore(audio_recognition_startTask, "Audio recognition task", 1024 * 4, NULL, 6, &audio_recog_startHandle, 1);
}


