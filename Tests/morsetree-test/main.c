
#include <stdlib.h>
#include <stdio.h>

#include "morsetree.h"

int main() {
    printf("Hello World!\n");

    morse_tree_handle_t tree = morse_tree_build();

    printf("Tree created!\n");

    printf("Decoding: --. (G)\n");
    char buffer[] = {1,1,0,1,1}; 
    char fromTree = morse_tree_get_char(tree, buffer, 3);

    printf("Got '%c' from tree!", fromTree);
    
    return 0;
}
