
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>

#include "esp_system.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "filter_resample.h"

#include "lcd.h"
#include "goertzel_filter.h"

#include "morsecode.h"
#include "morsetree.h"

static const char* LOG_TAG = "Morsecode";

#define MORSECODE_SHORT_BEEP 0
#define MORSECODE_LONG_BEEP 1

#define MORSECODE_BUFFER_LENGTH 6 // This is the length of the longest morsecode letter

#define GOERTZEL_SAMPLE_RATE_HZ 8000	// Sample rate in [Hz]

#define GOERTZEL_DETECTION_THRESHOLD 47.0f//47.0f  // Detect a tone when log magnitude is above this value

#define AUDIO_SAMPLE_RATE 48000         // Audio capture sample rate [Hz]

typedef enum {
    MORSE_BEEP,
    MORSE_CHAR,
} event_type_t;

typedef struct {
    event_type_t eventType;
    uint32_t data;
} morse_event_msg_t;

typedef struct {
    int morseShortBeep;
    int morseLongBeep;
    int morseMaxError;
} morse_timing_t;

static TaskHandle_t morseDetectTaskHandle;
static TaskHandle_t morseLcdWriteTaskHandle;
static bool morseDetectIsRunning = false;

static morse_tree_handle_t morseTree;

static QueueHandle_t morseEventQueue;

static void morse_detect_task(void* params);
static void morse_lcd_write_task(void* params);

static void beep_lcd_show(int beepLength, morse_timing_t* morseTiming);
static void morse_lcd_show(char decodedChar);

static morse_timing_t morsetiming_from_config(morse_config_t* config);

static audio_element_handle_t create_raw_stream();
static audio_element_handle_t create_i2s_stream(
    int sample_rate, 
    audio_stream_type_t type
);
static audio_element_handle_t create_resample_filter(
    int source_rate,
    int source_channels,
    int dest_rate,
    int dest_channels
);

static audio_pipeline_handle_t create_pipeline();

static char time_to_beep(int beepLength, morse_timing_t* morseTiming);

static void morse_event_dispatch(event_type_t eventType, int data);

#define MORSE_DETECT_TASK_STOPNOW() { \
        morseDetectIsRunning = false; \
        vTaskDelete(NULL); \
        return; \
    }

#define MORSE_DETECT_TASK_STOPGRACEFULL() { \
        morseDetectIsRunning = false; \
        break; \
    }

#define TICK_DELTA_IN_MS(start, stop) (((stop) - (start)) * portTICK_RATE_MS)

#define MORSE_DECODE_DISPATCH() { \
        decodedChar = morse_tree_get_char(morseTree, morseBuffer, morseBufferIndex); \
        morse_event_dispatch(MORSE_BEEP, -1); \
        morse_event_dispatch(MORSE_CHAR, decodedChar); \
        morseBufferIndex = 0; \
    }

/**
 * Determine if a frequency was detected or not, based on the magnitude that the
 * Goertzel filter calculated
 * Use a logarithm for the magnitude
 */
#define DETECT_FREQUENCY(magnitude) (10.0f * log10f(magnitude)) > GOERTZEL_DETECTION_THRESHOLD;

/**
 * @brief Checks if the specified deltaTime is like checkValue.
 *  This function takes MORSECODE_MAX_ERROR in consideration.
 * @param deltaTime The delta time (in milliseconds) to check.
 * @param checkValue The time value (in milliseconds) to check against.
 * @param morseTiming morse_timing_t
 */
#define MORSE_CHECK_TIME(deltaTime, checkValue, morseTiming) \
    (deltaTime) >= ((checkValue) - (morseTiming).morseMaxError) \
    && (deltaTime) <= ((checkValue) + (morseTiming).morseMaxError)

void morse_init() {
    
    // Initialize morse event queue
    morseEventQueue = xQueueCreate(10, sizeof(morse_event_msg_t));
    if(morseEventQueue == NULL) {
        ESP_LOGE(LOG_TAG, "Creating Morse Event Queue failed!");
        return;
    }

    vQueueAddToRegistry(morseEventQueue, "Morse Event Queue");

    // Initialize the morse decode tree
    morseTree = morse_tree_build();
    if(morseTree == NULL) {
        ESP_LOGE(LOG_TAG, "Creating Morse Decode Tree failed!");
        return;
    }

}

void morse_detect_start(morse_config_t* config) {

    if(!morseDetectIsRunning) {
        if(morseTree == NULL) {
            ESP_LOGE(LOG_TAG, "Morse detect is not initialised! Call morse_init() before morse_detect_start()");
            return;
        }

        if(config == NULL) {
            ESP_LOGE(LOG_TAG, "Morse config is NULL");
            return;
        }

        morse_config_t* configCpy = (morse_config_t*) malloc(sizeof(morse_config_t));
        if(configCpy == NULL) {
            ESP_LOGE(LOG_TAG, "Allocating space for morse config failed!");
            return;
        }

        if(config->speed < 10) {
            configCpy->speed = 10;
        }
        else if(config->speed > 30) {
            configCpy->speed = 30;
        }
        else {
            configCpy->speed = config->speed;
        }

        configCpy->frequency = config->frequency;

        ESP_LOGI(LOG_TAG, "Scheduling morse detect tasks...");

        morseDetectIsRunning = true;

        xTaskCreatePinnedToCore(morse_detect_task, "Morsecode Detect Task", 4096, configCpy, 6, &morseDetectTaskHandle, 1);
        xTaskCreate(morse_lcd_write_task, "Morsecode LCD Task", 2048, configCpy, 5, &morseLcdWriteTaskHandle);
    }
    else {
        ESP_LOGW(LOG_TAG, "Blocked trying to start a new morse detect task while it is already running!");
    }
    
}

bool morse_detect_is_running() {
    return morseDetectIsRunning;
}

void morse_detect_stop() {
    morseDetectIsRunning = false;
}

static void morse_detect_task(void* params) {
    morse_config_t* config = (morse_config_t*) params;

    esp_err_t error = ESP_OK;

    audio_pipeline_handle_t pipeline;

    audio_element_handle_t i2s_stream_reader;
    audio_element_handle_t resample_filter;
    audio_element_handle_t raw_reader;

    ESP_LOGI(LOG_TAG, "Loading morse detect config...");

    morse_timing_t morseTiming = morsetiming_from_config(config);

    int goertzelBufferLength = ((morseTiming.morseShortBeep / 4) * GOERTZEL_SAMPLE_RATE_HZ / 1000); // Buffer length in samples

    ESP_LOGI(LOG_TAG, "Initializing Goertzel detection filter for frequency %hdHz...", config->frequency);
    
    goertzel_filter_cfg_t gfilter_cfg;
    goertzel_filter_data_t gfilter_data;

    gfilter_cfg.sample_rate = GOERTZEL_SAMPLE_RATE_HZ;
    gfilter_cfg.target_freq = config->frequency;
    gfilter_cfg.buffer_length = goertzelBufferLength;

    error = goertzel_filter_setup(&gfilter_data, &gfilter_cfg);
    if(error != ESP_OK) {
        ESP_ERROR_CHECK_WITHOUT_ABORT(error);
        MORSE_DETECT_TASK_STOPNOW();
    }

    ESP_LOGI(LOG_TAG, "Initializing raw audio buffer...");

    // Goertzel filter + raw stream also needs a buffer for us to work with
    int16_t* raw_buffer = (int16_t*) malloc(goertzelBufferLength * sizeof(uint16_t));
    if(raw_buffer == NULL) {
        ESP_LOGE(LOG_TAG, "");
        MORSE_DETECT_TASK_STOPNOW();
    }

    ESP_LOGI(LOG_TAG, "Initializing audio elements (i2s, resample, raw) and audiopipeline...");

    pipeline = create_pipeline();

    i2s_stream_reader = create_i2s_stream(AUDIO_SAMPLE_RATE, AUDIO_STREAM_READER);
    resample_filter = create_resample_filter(AUDIO_SAMPLE_RATE, 2, GOERTZEL_SAMPLE_RATE_HZ, 1);
    raw_reader = create_raw_stream();

    const char* link_tag[3] = {"i2s", "rsp_filter", "raw"};
    audio_pipeline_register(pipeline, i2s_stream_reader, "i2s");
    audio_pipeline_register(pipeline, resample_filter, "rsp_filter");
    audio_pipeline_register(pipeline, raw_reader, "raw");

    audio_pipeline_link(pipeline, link_tag, 3);

    ESP_LOGI(LOG_TAG, "Starting audiopipeline and morsecode detection...");
    audio_pipeline_run(pipeline);

    lcd_takeMutex();

    lcd_setCursorToLine(1);
    lcd_write_string("Detecting...       ");

    lcd_giveMutex();

    ESP_LOGI(LOG_TAG, "Audiopipeline for morsecode detection with speed %hhdwpm started successfully! Listening for morsecode...", config->speed);

    bool morseCodeFound = false;
    bool silenceFound = false;
    bool frequencyDetected = false;

    TickType_t morseStartTime = 0;
    TickType_t morseStopTime = 0;

    TickType_t silenceStartTime = 0;
    TickType_t silenceStopTime = 0;

    TickType_t deltaTime = 0;

    char morseBuffer[MORSECODE_BUFFER_LENGTH];
    int morseBufferIndex = 0;
    char decodedChar = 0;

    float magnitude = 0.0f;

    while(morseDetectIsRunning) {
        raw_stream_read(raw_reader, (char*) raw_buffer, goertzelBufferLength * sizeof(int16_t));

        error = goertzel_filter_process(&gfilter_data, raw_buffer, goertzelBufferLength);
        if(error != ESP_OK) {
            ESP_ERROR_CHECK_WITHOUT_ABORT(error);
            MORSE_DETECT_TASK_STOPGRACEFULL();
        }

        if(goertzel_filter_new_magnitude(&gfilter_data, &magnitude)) {
            frequencyDetected = DETECT_FREQUENCY(magnitude);

            // If begin morsecode detected
            if(frequencyDetected && !morseCodeFound) {
                
                // Stop silence time
                if(silenceFound) {
                    silenceStopTime = xTaskGetTickCount();
                    deltaTime = TICK_DELTA_IN_MS(silenceStartTime, silenceStopTime);

                    // Check if silence is longer than LONGBEEP_LENGTH
                    // This marks the end of a letter
                    if (MORSE_CHECK_TIME(deltaTime, morseTiming.morseLongBeep, morseTiming) 
                        && morseBufferIndex > 0
                    ) {
                        MORSE_DECODE_DISPATCH();
                    }

                    silenceFound = false;
                }

                // Start morsecode time
                morseCodeFound = true;
                morseStartTime = xTaskGetTickCount();
                
            }   // If end morsecode detected
            else if(!frequencyDetected && morseCodeFound) {

                // Stop morsecode time
                morseStopTime = xTaskGetTickCount();

                deltaTime = TICK_DELTA_IN_MS(morseStartTime, morseStopTime);

                morseBuffer[morseBufferIndex++] = time_to_beep(deltaTime, &morseTiming);
                morse_event_dispatch(MORSE_BEEP, deltaTime);

                if(morseBufferIndex >= MORSECODE_BUFFER_LENGTH)
                    MORSE_DECODE_DISPATCH();

                morseStartTime = morseStopTime;
                morseCodeFound = false;

                // Start silence time
                silenceFound = true;
                silenceStartTime = xTaskGetTickCount();
            }
            
        }

        if(silenceFound) {
            silenceStopTime = xTaskGetTickCount();
            deltaTime = TICK_DELTA_IN_MS(silenceStartTime, silenceStopTime);

            // Check if silence is longer than LONGBEEP_LENGTH * 2
            // This marks the end of a word
            if(deltaTime >= (morseTiming.morseLongBeep * 2)) {

                // Decode char in morsebuffer (if any)
                if(morseBufferIndex > 0)
                    MORSE_DECODE_DISPATCH();

                morse_event_dispatch(MORSE_CHAR, ' ');

                // There is no longer silence than end of word, so stop detecting
                silenceFound = false;
                silenceStartTime = silenceStopTime;
            }
        }

    }

    ESP_LOGI(LOG_TAG, "Morsecode detection stop initiated, stopping audiopipeline and elements...");

    audio_pipeline_stop(pipeline);
    audio_pipeline_wait_for_stop(pipeline);
    audio_pipeline_terminate(pipeline);

    audio_pipeline_unregister(pipeline, i2s_stream_reader);
    audio_pipeline_unregister(pipeline, resample_filter);
    audio_pipeline_unregister(pipeline, raw_reader);

    audio_pipeline_deinit(pipeline);

    audio_element_deinit(i2s_stream_reader);
    audio_element_deinit(resample_filter);
    audio_element_deinit(raw_reader);

    ESP_LOGI(LOG_TAG, "Freeing up allocated memory for buffers and vars...");
    free(raw_buffer);
    free(config);

    MORSE_DETECT_TASK_STOPNOW();
}

static void morse_lcd_write_task(void* params) {
    morse_config_t* config = (morse_config_t*) params;

    morse_timing_t morseTiming = morsetiming_from_config(config);

    while(morseDetectIsRunning) {
        morse_event_msg_t event;

        if(xQueueReceive(morseEventQueue, &event, portMAX_DELAY)) {

            switch (event.eventType)
            {
                case MORSE_BEEP:
                    beep_lcd_show(event.data, &morseTiming);
                    break;
                case MORSE_CHAR:
                    morse_lcd_show((char)event.data);
                    break;
                default:
                    break;
            }

        }

        vTaskDelay(1 / portTICK_RATE_MS);
    }

    vTaskDelete(NULL);

}

/**
 * Shows the beeps of the morse code on the lcd
 * Resets to a new morseletter when -1 is provided as beepLength
 * @param beepLength The length of the beep, resets when -1
 */
static void beep_lcd_show(int beepLength, morse_timing_t* morseTiming) {
    static int cursorPos = 0;

    // Reset when beepLength = -1
    if(beepLength == -1) {

        lcd_takeMutex();

        lcd_setCursorToLine(2);
        lcd_write_string("                    ");

        lcd_giveMutex();

        cursorPos = 0;
        return;
    }

    char beepChar;
    if(MORSE_CHECK_TIME(beepLength, morseTiming->morseShortBeep, *morseTiming)) {
        // Short beep = .
        beepChar = '.';
    }
    else if(MORSE_CHECK_TIME(beepLength, morseTiming->morseLongBeep, *morseTiming)) {
        // Long beep = -
        beepChar = '-';
    }
    else {
        // Inaccurate guess
        if((beepLength < (morseTiming->morseLongBeep / 2))){
            // Inaccurate short beep = *
            beepChar = '*';
        }
        else {
            // Inaccurate long beep = '='
            beepChar = '=';
        }
    }

    char writeBuffer[] = { beepChar, 0 };

    lcd_takeMutex();

    lcd_setCursor(cursorPos, 2);
    lcd_write_string(writeBuffer);

    lcd_giveMutex();

    cursorPos++;
}

/**
 * Shows the char decoded from morse on the LCD
 * Resets when decodedChar has the value '\0' (0)
 * @param decodedChar The char to show, resets when '\0'
 */
static void morse_lcd_show(char decodedChar) {
    static int cursorPos = 0;
    static char* lcdBuffer;

    // Reset when decodedChar = 0
    if(decodedChar == 0) {
        static const char* clearLineString = "                    ";

        // Clear LCD
        lcd_takeMutex();

        lcd_setCursorToLine(3);
        lcd_write_string(clearLineString);

        lcd_giveMutex();

        // Clear lcdBuffer (if created)
        if(lcdBuffer != NULL) {
            strcpy(lcdBuffer, clearLineString);
        }
        
        // Reset cursor position
        cursorPos = 0;
        return;
    }

    // Initialize buffer (if needed)
    if(lcdBuffer == NULL) {
        lcdBuffer = calloc(sizeof(char), 21 /* Display Line Length + NULL */);

        if(lcdBuffer == NULL) {
            ESP_LOGE(LOG_TAG, "Allocating memory for lcdBuffer (morse message) failed");
            return;
        }
    }

    // If the cursor position is at the end of the screen
    if(cursorPos >= 20 /* Display Line Lenght*/) {

        // Scroll string to 1 the left

        for(int i = 1; i < 20 /* Display Line Lenght*/; i++) {
            lcdBuffer[i - 1] = lcdBuffer[i];
        }
        
        // Append new character
        lcdBuffer[19 /* Last Character*/] = decodedChar;
    }
    else {
        // Just append to the end of the string
        lcdBuffer[cursorPos] = decodedChar;
        cursorPos++;
    }

    // Write message to lcd
    lcd_takeMutex();

    lcd_setCursor(0, 3);
    lcd_write_string(lcdBuffer);

    lcd_giveMutex();
}

static morse_timing_t morsetiming_from_config(morse_config_t* config) {
    morse_timing_t morseTiming;

    // Formula for morseBeepLength:
    // https://en.wikipedia.org/wiki/Morse_code#:~:text=Based%20upon%20a%2050%C2%A0dot%20duration%20standard%20word%20such%20as%20paris%2C%20the%20time%20for%20one%20dit%20duration%20or%20one%20unit%20can%20be%20computed%20by%20the%20formula%3A
    morseTiming.morseShortBeep = 1200 / config->speed; // Speed in milliseconds
    morseTiming.morseLongBeep = morseTiming.morseShortBeep * 3; // Per definition: Long beep is 3 times as long as short beep
    morseTiming.morseMaxError = morseTiming.morseShortBeep / 3; // in milliseconds

    return morseTiming;
}

/**
 * Creates the i2s stream with the audio chip
 */
static audio_element_handle_t create_i2s_stream(
    int sample_rate, 
    audio_stream_type_t type
) {
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = type;
    i2s_cfg.i2s_config.sample_rate = sample_rate;
    audio_element_handle_t i2s_stream = i2s_stream_init(&i2s_cfg);
    return i2s_stream;
}

/**
 * Creates a resample filter to sample down for goertzel function
 */
static audio_element_handle_t create_resample_filter(
    int source_rate,
    int source_channels,
    int dest_rate,
    int dest_channels
) {
    rsp_filter_cfg_t rsp_cfg = DEFAULT_RESAMPLE_FILTER_CONFIG();
    rsp_cfg.src_rate = source_rate;
    rsp_cfg.src_ch = source_channels;
    rsp_cfg.dest_rate = dest_rate;
    rsp_cfg.dest_ch = dest_channels;
    audio_element_handle_t filter = rsp_filter_init(&rsp_cfg);
    return filter;
}

/**
 * Creates a raw stream to be used by the morsedetect
 */
static audio_element_handle_t create_raw_stream() {
    raw_stream_cfg_t raw_cfg = {
        .out_rb_size = 8 * 1024,
        .type = AUDIO_STREAM_READER,
    };
    audio_element_handle_t raw_reader = raw_stream_init(&raw_cfg);
    return raw_reader;
}

/**
 * creates a pipeline which can be used for the morsecode detection
 */
static audio_pipeline_handle_t create_pipeline() {
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    audio_pipeline_handle_t pipeline = audio_pipeline_init(&pipeline_cfg);
    return pipeline;
}

/**
 * Converts a beep length time to an beep for the decoder
 * @param beepLength The length of the beep in milliseconds
 */
static char time_to_beep(int beepLength, morse_timing_t* morseTiming) {
    if(MORSE_CHECK_TIME(beepLength, morseTiming->morseShortBeep, *morseTiming)) {
        return MORSECODE_SHORT_BEEP;
    }
    else if(MORSE_CHECK_TIME(beepLength, morseTiming->morseLongBeep, *morseTiming)) {
        return MORSECODE_LONG_BEEP;
    }
    else {
        return (beepLength < (morseTiming->morseLongBeep / 2)) ? MORSECODE_SHORT_BEEP : MORSECODE_LONG_BEEP;
    }
}

/**
 * Dispatches an event to the event queue for processing on the LCD
 * @param eventType The type of the event
 * @param data Event data
 */
static void morse_event_dispatch(event_type_t eventType, int data) {
    morse_event_msg_t event = {
        .eventType = eventType,
        .data = data
    };

    // xQueueSendToBack makes a copy of event contents
    // so we can pass a local pointer
    xQueueSendToBack(morseEventQueue, &event, 0 /* Don't wait if the queue is full */);
}

