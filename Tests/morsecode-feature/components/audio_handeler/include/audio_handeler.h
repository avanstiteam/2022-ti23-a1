#pragma once

#define AUDIO_PIPE_NON 0
#define AUDIO_PIPE_USERADIO 1
#define AUDIO_PIPE_USESD 2

#include "audio_pipeline.h"

typedef int audio_type;
extern audio_pipeline_handle_t pipeline;

void audio_handler_pipeline_init(void);
/**
 * @brief Can be used to start playing audio. The pipeline needs to be initialized before this call.
 * 
 * @param type Is an audio_type of which you want to play.
 * @param url Is the url of the file / link.
 * 
 * Radio starts it's own task.
 * SD is a blocking call. This should be called from an external task.
 */
void audio_handler_playAudio(audio_type type, char * url);
void audio_handler_stopAudio(void);
void audio_handler_muteRadio();