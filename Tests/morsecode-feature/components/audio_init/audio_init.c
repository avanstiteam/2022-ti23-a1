#include <stdio.h>

#include "audio_init.h"

void audio_init_initialize(void) {
    audio_board_handle = audio_board_init();
    audio_hal_ctrl_codec(audio_board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);
    audio_hal_set_volume(audio_board_handle->audio_hal, AUDIO_HAL_VOL_DEFAULT);
}
