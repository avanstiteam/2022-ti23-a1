
#include <stdio.h>
#include <sys/time.h>

#include "esp_system.h"
#include "esp_log.h"

#include "audio_init.h"
#include "audio_handeler.h"
#include "lcd.h"
#include "morsecode.h"

void app_main()
{
    audio_init_initialize();
    //audio_handler_pipeline_init();

    lcd_init();

    lcd_takeMutex();

    lcd_setCursorToLine(0);
    lcd_write_string("Morsecode");

    lcd_setCursorToLine(1);
    lcd_write_string("Initializing...");

    lcd_giveMutex();

    morse_config_t config = {
        .speed = 30,
        .frequency = 900 // TODO: Add support for 500, 550, 700, 900
    };

    morse_init();
    morse_detect_start(&config);
}