
#ifndef WIFI_H
#define WIFI_H

esp_err_t wifi_init();

esp_err_t wifi_connect();

#endif