#ifndef LCD_H

#define LCD_H

void lcd_setCursor(int column, int row);
void lcd_setCursorToLine(int line);
void lcd_write_string(const char *str);
void lcd_init();
void lcd_clear();

#endif