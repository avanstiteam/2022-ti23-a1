/*

    Remarks: sntp updates the internal clock once an hour by default.
        This behaviour can be changed in menuconfig under:
            (Top) -> Component Config -> LWIP -> SNTP

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "esp_system.h"
#include "esp_sntp.h"
#include "esp_log.h"

#include "ntp.h"

static const char* LOG_TAG = "ntp";

static void (*ntp_sync_callback)(struct timeval*);

static void ntp_time_sync_callback(struct timeval* tv) {
    time_t syncResult = tv->tv_sec;

    struct tm timeInfo;
    localtime_r(&syncResult, &timeInfo);

    ESP_LOGI(LOG_TAG, "Got sync update from ntp server. Adjusting time to: %02d-%02d-%04d %02d:%02d:%02d",
        timeInfo.tm_mday,
        timeInfo.tm_mon + 1,
        timeInfo.tm_year + 1900,
        timeInfo.tm_hour,
        timeInfo.tm_min,
        timeInfo.tm_sec
    );

    if(ntp_sync_callback != NULL) 
        (*ntp_sync_callback)(tv);
}

void ntp_init(void (*sync_callback)(struct timeval*)) {
    sntp_servermode_dhcp(1);

    // Source for TZ environment variabele value:
    // https://unix.stackexchange.com/questions/19935/timezone-time-setting-in-linux-using-shellscript/19957#19957
    setenv("TZ", "CET-1CEST,M3.5.0/2,M10.5.0/3", 1);
    tzset();

    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_set_sync_mode(SNTP_SYNC_MODE_SMOOTH);
    sntp_setservername(0, "pool.ntp.org");

    ntp_sync_callback = sync_callback;
    sntp_set_time_sync_notification_cb(ntp_time_sync_callback);

    sntp_init();
}
