
#include <stdio.h>
#include <time.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_system.h"
#include "nvs_flash.h"

#include "lcd.h"
#include "wifi.h"
#include "ntp.h"

#define millis(x) ((x) / portTICK_RATE_MS)

void print_time(struct tm* timeInfo);

void clock_task(void* parameters) {

    TickType_t lastWakeTime = 0;

    time_t now;
    struct tm timeInfo;

    time(&now);
    localtime_r(&now, &timeInfo);

    while(1) {
        time(&now);
        localtime_r(&now, &timeInfo);

        print_time(&timeInfo);

        vTaskDelayUntil(&lastWakeTime, millis(1000));
    }

    vTaskDelete(NULL);
}

void ntp_callback(struct timeval* tv) {
    printf("Hello from callback!\n");
}

void app_main()
{
    nvs_flash_init();

    wifi_init();
    wifi_connect();

    lcd_init();

    vTaskDelay(2000 / portTICK_RATE_MS);

    printf("Starting ntp...\n");
    ntp_init(&ntp_callback);

    xTaskCreate(clock_task, "Clock Task", 2048, NULL, 1, NULL);
}

void print_time(struct tm* timeInfo) {
    static char strBuffer[100];
    sprintf(strBuffer, "%02d-%02d-%04d %02d:%02d:%02d",
        timeInfo->tm_mday,
        timeInfo->tm_mon + 1,
        timeInfo->tm_year + 1900,
        timeInfo->tm_hour,
        timeInfo->tm_min,
        timeInfo->tm_sec
    );

    lcd_setCursorToLine(0);
    lcd_write_string(strBuffer);

    printf("Datetime is: %s\n", strBuffer);
}