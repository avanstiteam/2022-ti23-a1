
#include <stdio.h>
#include <stdbool.h>

#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "wifi.h"

const char* LOG_TAG = "WiFi";

#define esp_assert(cond, msg) if((result = (cond)) != ESP_OK) { \
    ESP_LOGE(LOG_TAG, msg " => %s", esp_err_to_name(result)); \
    return result; \
}

esp_err_t wifi_init() {
    esp_err_t result = ESP_OK;

    esp_assert(esp_netif_init(), "wifi_init(): esp_netifinit() failed");

    result = esp_event_loop_create_default();
    if(!(result == ESP_OK || result == ESP_ERR_INVALID_STATE)) {
        ESP_LOGE(LOG_TAG, "wifi_init(): esp_event_loop_create_default() failed => %s", esp_err_to_name(result));
    }

    esp_netif_create_default_wifi_sta();

    wifi_init_config_t wifiInitConfig = WIFI_INIT_CONFIG_DEFAULT();
    esp_assert(esp_wifi_init(&wifiInitConfig), "wifi_init(): esp_wifi_init() failed");

    esp_assert(esp_wifi_set_default_wifi_sta_handlers(), "wifi_init(): esp_wifi_set_default_wifi_sta_handlers() failed");

    return result;
}

esp_err_t wifi_connect() {
    esp_err_t result = ESP_OK;
    
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = CONFIG_WIFI_SSID,
            .password = CONFIG_WIFI_PASSWORD
        },
    };

    esp_assert(esp_wifi_set_mode(WIFI_MODE_STA), "wifi_start(): esp_wifi_set_mode(WIFI_MODE_STA) failed");
    esp_assert(esp_wifi_set_config(WIFI_IF_STA, &wifi_config), "wifi_start(): esp_wifi_set_config() failed");
    
    esp_assert(esp_wifi_start(), "wifi_start(): esp_wifi_start() failed")
    esp_assert(esp_wifi_connect(), "wifi_start(): esp_wifi_connect() failed")

    return result;
}