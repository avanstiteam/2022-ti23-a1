
#include <stdio.h>

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "nvs_flash.h"

#include "wifi.h"

void app_main()
{
    printf("Connect Wi-Fi\n");

    nvs_flash_init();

    printf("Initializing Wi-Fi...\n");
    wifi_init();

    printf("Connecting to Wi-Fi...\n");
    wifi_connect();

    printf("We should be connected now!\n");
}