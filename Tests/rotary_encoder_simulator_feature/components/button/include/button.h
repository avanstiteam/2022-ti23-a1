
#ifndef BUTTON_H
#define BUTTON_H

#include "extgpio.h"

typedef void* button_handle_t;

typedef void (*ButtonPressCallback_t)(button_handle_t btn);
typedef void (*ButtonClickCallback_t)(button_handle_t btn);
typedef void (*ButtonReleaseCallback_t)(button_handle_t btn, uint32_t pressTimeMillis);

button_handle_t btn_create(extgpio_port port, uint8_t pin);
void btn_delete(button_handle_t* btnHandle);

void btn_register_press_handler(button_handle_t* btnHandle, ButtonPressCallback_t pressCallback);
void btn_register_click_handler(button_handle_t* btnHandle, ButtonClickCallback_t clickCallback);
void btn_register_longclick_handler(button_handle_t* btnHandle, ButtonClickCallback_t longClickCallback);
void btn_register_release_handler(button_handle_t* btnHandle, ButtonReleaseCallback_t releaseCallback);

#endif
