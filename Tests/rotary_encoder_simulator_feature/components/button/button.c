
#include <stdlib.h>
#include <stdio.h>

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

#include "extgpio.h"

#include "button.h"

static const char* LOG_TAG = "BUTTON";

typedef struct button {
    extgpio_port gpioPort;
    uint8_t gpioPin;
    TaskHandle_t task;
    SemaphoreHandle_t mutex;
    ButtonPressCallback_t pressCallback;
    ButtonClickCallback_t clickCallback;
    ButtonClickCallback_t longClickCallback;
    ButtonReleaseCallback_t releaseCallback;
} button_t;

static void btn_task(void* parameter);

button_handle_t btn_create(extgpio_port port, uint8_t pin) {

    ESP_LOGI(LOG_TAG, "Initializing button on %s, pin 0x%02X", extgpio_portname(port), pin);

    button_t* btn = (button_t*) malloc(sizeof(button_t));

    if(btn == NULL) {
        ESP_LOGE(LOG_TAG, "Initializing button on %s, pin 0x%02X failed", extgpio_portname(port), pin);
        return NULL;
    }

    btn->gpioPort = port;
    btn->gpioPin = pin;

    btn->pressCallback = NULL;
    btn->clickCallback = NULL;
    btn->releaseCallback = NULL;

    btn->mutex = xSemaphoreCreateMutex();
    xSemaphoreGive(btn->mutex);

    extgpio_set_pin_direction_in(btn->gpioPort, btn->gpioPin);

    xTaskCreate(btn_task, "Button Task", 2048, (void*)btn, 5, &btn->task);

    return (button_handle_t) btn;
}

void btn_delete(button_handle_t* btnHandle) {
    button_t* btn = (button_t*) (*btnHandle);

    vTaskDelete(btn->task);
    
    free(*btnHandle);
    *btnHandle = NULL;
}

void btn_register_press_handler(button_handle_t* btnHandle, ButtonPressCallback_t pressCallback) {
    button_t* btn = (button_t*)btnHandle;

    xSemaphoreTake(btn->mutex, portMAX_DELAY);
    btn->pressCallback = pressCallback;
    xSemaphoreGive(btn->mutex);
}

void btn_register_click_handler(button_handle_t* btnHandle, ButtonClickCallback_t clickCallback) {
    button_t* btn = (button_t*)btnHandle;

    xSemaphoreTake(btn->mutex, portMAX_DELAY);
    btn->clickCallback = clickCallback;
    xSemaphoreGive(btn->mutex);
}

void btn_register_longclick_handler(button_handle_t* btnHandle, ButtonClickCallback_t longClickCallback) {
    button_t* btn = (button_t*)btnHandle;

    xSemaphoreTake(btn->mutex, portMAX_DELAY);
    btn->longClickCallback = longClickCallback;
    xSemaphoreGive(btn->mutex);
}

void btn_register_release_handler(button_handle_t* btnHandle, ButtonReleaseCallback_t releaseCallback) {
    button_t* btn = (button_t*)btnHandle;

    xSemaphoreTake(btn->mutex, portMAX_DELAY);
    btn->releaseCallback = releaseCallback;
    xSemaphoreGive(btn->mutex);
}

static void btn_task(void* parameter) {
    button_t* btn = (button_t*) parameter;

    bool buttonPressedState = false;
    bool lastButtonPressedState = false;
    bool longClickProcessed = false;

    TickType_t pressStartTime = 0;
    TickType_t currentTick = 0;
    TickType_t pressDeltaTime = 0;

    while(1) {
        buttonPressedState = extgpio_get_pin(btn->gpioPort, btn->gpioPin);

        currentTick = xTaskGetTickCount();

        if(buttonPressedState != lastButtonPressedState) {
            // Button state changed

            if(buttonPressedState) {
                // Button is pressed
                pressStartTime = xTaskGetTickCount();

                xSemaphoreTake(btn->mutex, portMAX_DELAY);

                if(btn->pressCallback != NULL)
                    (*btn->pressCallback)((button_handle_t)btn);

                xSemaphoreGive(btn->mutex);
            }
            else {
                // Button is released
                pressDeltaTime = (currentTick - pressStartTime) * portTICK_RATE_MS;

                xSemaphoreTake(btn->mutex, portMAX_DELAY);
                if(!longClickProcessed && btn->clickCallback != NULL)
                    (*btn->clickCallback)((button_handle_t)btn);

                if(btn->releaseCallback != NULL) {
                    (*btn->releaseCallback)(
                        (button_handle_t) btn,
                        (uint32_t) pressDeltaTime
                    );
                }
                xSemaphoreGive(btn->mutex);
                
                longClickProcessed = false;
                pressStartTime = 0;
                pressDeltaTime = 0;
            }

            // wait a bit to debounce
            vTaskDelay(10 / portTICK_RATE_MS);
        }

        if(buttonPressedState && !longClickProcessed && ((currentTick - pressStartTime) * portTICK_RATE_MS) > 500) {
            xSemaphoreTake(btn->mutex, portMAX_DELAY);

            if(btn->longClickCallback != NULL)
                (*btn->longClickCallback)((button_handle_t)btn);

            xSemaphoreGive(btn->mutex);

            longClickProcessed = true;
        }

        lastButtonPressedState = buttonPressedState;
        vTaskDelay(10 / portTICK_RATE_MS);
    }

    vTaskDelete(NULL);
}