
#include <stdio.h>
#include <stdbool.h>

#include "esp_log.h"
#include "mcp23017.h"

#include "extgpio.h"

static const char* LOG_TAG = "EXTGPIO";

static mcp23017_handle_t mcp_dev;

static uint8_t gpio_value_list[] = {
    0x00,
    0x00
};

static uint8_t gpio_direction_list[] = {
    0x00,
    0x00
};

void extgpio_init() {
    mcp_dev = mcp23017_create(MCP23017_I2C_ADDRESS_DEFAULT);

    if(mcp_dev == NULL) {
        ESP_LOGE(LOG_TAG, "Could not create mcp23017 device at address 0x%02X", MCP23017_I2C_ADDRESS_DEFAULT);
    }

    esp_err_t err = mcp23017_check_present(mcp_dev);
    if(err != ESP_OK) {
        ESP_LOGE(LOG_TAG, "Could not create mcp23017 device at address 0x%02X, device not found!", MCP23017_I2C_ADDRESS_DEFAULT);
        mcp23017_delete(mcp_dev);
        mcp_dev = NULL;
    }

    extgpio_set_direction(GPIOA, 0x00);
    extgpio_set_direction(GPIOB, 0x00);

    extgpio_set(GPIOA, 0x00);
    extgpio_set(GPIOB, 0x00);
}

void extgpio_set_direction(mcp23017_gpio_port_t gpio, uint8_t value) {
    esp_err_t err = mcp23017_set_io_dir(mcp_dev, value, gpio);

    if(err == ESP_OK) {
        gpio_direction_list[gpio] = value;
    }
    else {
        ESP_LOGE(LOG_TAG, "Could not set direction for %s to 0x%02X", extgpio_portname(gpio), value);
    }
}

void extgpio_set_pin_direction_in(mcp23017_gpio_port_t gpio, uint8_t pin) {
    extgpio_set_direction(gpio, gpio_direction_list[gpio] | pin);
}

void extgpio_set_pin_direction_out(mcp23017_gpio_port_t gpio, uint8_t pin) {
    extgpio_set_direction(gpio, gpio_direction_list[gpio] & ~pin);
}

void extgpio_set(mcp23017_gpio_port_t gpio, uint8_t value) {
    esp_err_t err = mcp23017_write_io(mcp_dev, value, gpio);

    if(err == ESP_OK) {
        gpio_value_list[gpio] = value;
    }
    else {
        ESP_LOGE(LOG_TAG, "Could not set value for %s to 0x%02X", extgpio_portname(gpio), value);
    }
}

void extgpio_set_pin_on(mcp23017_gpio_port_t gpio, uint8_t pin) {
    extgpio_set(gpio, gpio_value_list[gpio] | pin);
}

void extgpio_set_pin_off(mcp23017_gpio_port_t gpio, uint8_t pin) {
    extgpio_set(gpio, gpio_value_list[gpio] & ~pin);
}

uint8_t extgpio_get(mcp23017_gpio_port_t gpio) {
    return mcp23017_read_io(mcp_dev, gpio);
}

bool extgpio_get_pin(mcp23017_gpio_port_t gpio, uint8_t pin) {
    return mcp23017_read_io(mcp_dev, gpio) & pin ? true : false;
}