
#include <stdio.h>

#include "esp_system.h"
#include "driver/i2c.h"

#include "extgpio.h"
#include "button.h"

#ifndef LYRAT_SCK
#define LYRAT_SCK 23
#endif
#ifndef LYRAT_SDA
#define LYRAT_SDA 18
#endif

#define LCD_NUM_ROWS               4
#define LCD_NUM_COLUMNS            16
#define LCD_NUM_VISIBLE_COLUMNS    20

#define I2C_MASTER_NUM           I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled
#define I2C_MASTER_FREQ_HZ       100000
#define I2C_MASTER_SDA_IO        LYRAT_SDA
#define I2C_MASTER_SCL_IO        LYRAT_SCK
#ifndef LYRAT_SCK_SPEED
#define LYRAT_SCK_SPEED 10000
#endif

button_handle_t button1;
button_handle_t button2;
button_handle_t button3;


i2c_config_t i2c_init() {
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(I2C_MASTER_NUM, &conf);
    i2c_driver_install(I2C_MASTER_NUM, conf.mode,
                       I2C_MASTER_RX_BUF_LEN,
                       I2C_MASTER_TX_BUF_LEN, 0);
    return conf;
}

uint8_t button_get_number(button_handle_t button) {
    if(button == button1)
        return 1;
    
    if(button == button2)
        return 2;
    
    if(button == button3)
        return 3;
    
    return 0;
}

void button_pressed_handler(button_handle_t button) {
    printf("Button %d Pressed!\n", button_get_number(button));
}

void button_click_handler(button_handle_t button) {
    printf("Button %d Click\n", button_get_number(button));
}

void button_longclick_handler(button_handle_t button) {
    printf("Button %d Long-Click\n", button_get_number(button));
}

void button_release_handler(button_handle_t button, uint32_t pressTimeMillis) {
    printf("Button %d Release! Press Time: %d millis\n", button_get_number(button), pressTimeMillis);
}

void app_main()
{
    i2c_config_t i2c_conf = i2c_init();
    extgpio_init();

    button1 = btn_create(GPIOA, extgpio_pin(0));
    button2 = btn_create(GPIOA, extgpio_pin(1));
    button3 = btn_create(GPIOA, extgpio_pin(2));

    btn_register_press_handler(button1, button_pressed_handler);
    btn_register_click_handler(button1, button_click_handler);
    btn_register_release_handler(button1, button_release_handler);

    btn_register_press_handler(button2, button_pressed_handler);
    btn_register_click_handler(button2, button_click_handler);
    btn_register_longclick_handler(button2, button_longclick_handler);
    btn_register_release_handler(button2, button_release_handler);

    btn_register_press_handler(button3, button_pressed_handler);
    btn_register_click_handler(button3, button_click_handler);
    btn_register_release_handler(button3, button_release_handler);
}