﻿
var tree = new MorseDecodeTree.MorseDecodeTree();

// From ROOT
tree.AddNode(new bool[] { false }, 'E');
tree.AddNode(new bool[] { false, false }, 'I');
tree.AddNode(new bool[] { false, false, false }, 'S');
tree.AddNode(new bool[] { false, false, false, false }, 'H');
tree.AddNode(new bool[] { false, false, false, false, false }, '5');

// from H
tree.AddNode(new bool[] { false, false, false, false, true }, '4');

// from S
tree.AddNode(new bool[] { false, false, false, true }, 'V');
tree.AddNode(new bool[] { false, false, false, true, true }, '3');

// from I
tree.AddNode(new bool[] { false, false, true }, 'U');
tree.AddNode(new bool[] { false, false, true, false }, 'F');

// from U
tree.AddNode(new bool[] { false, false, true, true, true }, '2');

// from E
tree.AddNode(new bool[] { false, true }, 'A');
tree.AddNode(new bool[] { false, true, false }, 'R');
tree.AddNode(new bool[] { false, true, false, false }, 'L');

// from A
tree.AddNode(new bool[] { false, true, true }, 'W');
tree.AddNode(new bool[] { false, true, true, false }, 'P');

// from W
tree.AddNode(new bool[] { false, true, true, true }, 'J');
tree.AddNode(new bool[] { false, true, true, true, true }, '1');



// from ROOT
tree.AddNode(new bool[] { true }, 'T');
tree.AddNode(new bool[] { true, false }, 'N');
tree.AddNode(new bool[] { true, false, false }, 'D');
tree.AddNode(new bool[] { true, false, false, false }, 'B');
tree.AddNode(new bool[] { true, false, false, false, false }, '6');

// from D
tree.AddNode(new bool[] { true, false, false, true }, 'X');
tree.AddNode(new bool[] { true, false, false, true, false }, '/');

// from N
tree.AddNode(new bool[] { true, false, true }, 'K');
tree.AddNode(new bool[] { true, false, true, false }, 'C');

// from K
tree.AddNode(new bool[] { true, false, true, true }, 'Y');
tree.AddNode(new bool[] { true, false, true, true, false }, '(');

//from T
tree.AddNode(new bool[] { true, true }, 'M');
tree.AddNode(new bool[] { true, true, false }, 'G');
tree.AddNode(new bool[] { true, true, false, false }, 'Z');
tree.AddNode(new bool[] { true, true, false, false, false }, '7');

// from G
tree.AddNode(new bool[] { true, true, false, true }, 'Q');

// from M
tree.AddNode(new bool[] { true, true, true }, 'O');
tree.AddNode(new bool[] { true, true, true, false, false }, '8');

// from O
tree.AddNode(new bool[] { true, true, true, true, false }, '9');
tree.AddNode(new bool[] { true, true, true, true, true }, '0');


tree.AddNode(new bool[] { false, false, false, false, false, false }, ',');


// Miscellaneaus
tree.AddNode(new bool[] { false, true, false, true, false, true }, '.');
tree.AddNode(new bool[] { true, true, false, false, true, true }, ',');
tree.AddNode(new bool[] { false, false, true, true, false, false }, '?');
tree.AddNode(new bool[] { true, false, false, false, false, true }, '-');
tree.AddNode(new bool[] { true, true, true, false, false, false }, ':');
tree.AddNode(new bool[] { false, true, true, true, true, false }, '\'');
tree.AddNode(new bool[] { true, false, true, true, false, true }, ')');
tree.AddNode(new bool[] { true, false, true, false, true }, ';');
tree.AddNode(new bool[] { true, false, false, false, true }, '=');
tree.AddNode(new bool[] { false, true, true, false, true, false }, '@');
tree.AddNode(new bool[] { false, true, false, false, false }, '&');
tree.AddNode(new bool[] { true, false, true, false, true, true }, '!');

tree.DumpTree();