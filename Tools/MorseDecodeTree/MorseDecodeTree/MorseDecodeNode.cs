﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseDecodeTree
{
    internal record MorseDecodeNode(char character)
    {

        public MorseDecodeNode? shortLeaf;
        public MorseDecodeNode? longLeaf;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.character == '\0' ? '?' : this.character);

            sb.Append(shortLeaf is not null ? shortLeaf.ToString() : ' ');
            sb.Append(longLeaf is not null ? longLeaf.ToString() : ' ');

            return sb.ToString();
        }

    }
}
